<div class="container">
  <div class="row">
    <div class="col-12 mt-3 text-center text-uppercase">
      <h2>Cadastro de Produtos</h2>
    </div>
  </div>
  <form action="" method="POST" name="formulario_cadastro_produto" id="formulario_cadastro_produto" data-toggle="validator">

    <div class="form-row">
      <div class="   col-md-8 ">
        <label> Nome do Produto:</label>
        <div class="iconInput">
          <i class="fa fa-adn"></i>
          <input type="text" name="produtoNome" id="produtoNome" class="form-control " placeholder="Digite o nome do produto">
          <span class="help-block"></span>
        </div>
      </div>
    </div>

    <br>
    <div class="form-row">
      <div class="form-group col-md-4 ">
        <label for="inputCity">Valor do Produto:</label>
        <div class="iconInput">
          <i class="fa fa-dollar-sign"></i>
          <input type="text" name="produtoValor" id="produtoValor" class="form-control" placeholder="Digite o valor do produto">
        </div>
      </div>
    </div>
    <div class="form-row">
      <div class="col-md-8">
        <label> Produto está em promocão?</label>
        <div class="iconInput">
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="produto_promocao" id="produto_promocao0" value="0" checked>
            <label class="form-check-label" for="inlineRadio1">Não</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="produto_promocao" id="produto_promocao1" value="1">
            <label class="form-check-label" for="inlineRadio2">Sim</label>
          </div>

        </div>
      </div>
    </div>
    <div id="promocao">

    </div>
    <br>

    <div class="form-row">
      <div class="col-md-6">
        <label> Descricao:</label>
        <div class="iconInput">
          <textarea name="produtoDescricao" id="produtoDescricao" class="col-md-12">

              </textarea>

        </div>
      </div>
    </div>

    <div class="form-row" style="margin-top: 2%;">
      <div class="form-group col-md-3 ">
        <label for="inputCity">Categoria:</label>
        <select id="produtoCategoria" name="produtoCategoria" class="form-control">
        <option selected disabled>Selecione...</option>
          <?php
          foreach ($categorias as $categoria) {
            echo '
              <option value="'.$categoria['categoriaId'].'">'.$categoria['categoriaNome'].'</option>
            ';
          }
          ?>

        </select>
      </div>
      <div class="form-group col-md-3">
        <label for="inputCity">Sub Categoria:</label>
        <select id="constroi" name="subCategoriaId" class="form-control">
          <option selected disabled>Selecione...</option>
          

        </select>
      </div>
    </div>
    
    <div class="form-row">
      <div class="form-group col-md-3 ">
        <label for="inputCity">Imagem Principal:</label>
        <div>
          <img id="produto_imagem_path" src="" style="max-width: 150px;max-height: 150px;">
          <label class="btn btn-block btn-info">
            <i class="fa fa-plus"></i>&nbsp;&nbsp;Imagem principal
            <input type="file" id="btn_upload_imagem_produto" accept="image/*" style="display: none;" produto="0">
          </label>
          <input type="text" id="produtoImagemPrincipal" name="produtoImagemPrincipal" hidden>
          <span class="help-block"></span>
        </div>
      </div>
    </div><br>

    <div class="form-row">
      <div class="form-group col-md-3 ">
        <label for="inputCity">Imagem 1:</label>
        <div>
          <img id="produto1_imagem_path" src="" style="max-width: 150px;max-height: 150px;">
          <label class="btn btn-block btn-info">
            <i class="fa fa-plus"></i>&nbsp;&nbsp;Imagem I
            <input type="file" id="btn_upload_imagem_produto1" accept="image/*" style="display: none;" produto="1">
          </label>
          <input type="text" id="produtoImagem1" name="produtoImagem1" hidden>
          <span class="help-block"></span>
        </div>
      </div>
      <div class="form-group col-md-3">
        <label for="inputCity">Imagem 2:</label>
        <div>
          <img id="produto2_imagem_path" src="" style="max-width: 150px;max-height: 150px;">
          <label class="btn btn-block btn-info">
            <i class="fa fa-plus"></i>&nbsp;&nbsp;Imagem II
            <input type="file" id="btn_upload_imagem_produto2" accept="image/*" style="display: none;" produto="2">
          </label>
          <input type="text" id="produtoImagem2" name="produtoImagem2" hidden>
          <span class="help-block"></span>
        </div>
      </div>
    </div>

    <br>

    <div class="form-row ">
      <div>
        <button type="submit" id="btn_salvar_produto" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar</button>
        <span class="help-block"></span>
      </div>
    </div><br>
    <div id="cadastrando">
    </div>
    <br>


  </form>

  <!-- /.content -->
</div>