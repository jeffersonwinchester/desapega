<div class="col-12">
  <!-- Main Content -->
  <div class="row">
    <div class="col-12 mt-3 text-center text-uppercase">
      <h2>Meus Dados</h2>
    </div>
  </div>

  <main class="row">

    <div class="col-12 bg-white py-3 mb-3">
     

      <form action="" method="POST" name="formulario_editar_usuario" id="formulario_editar_usuario" data-toggle="validator">
      <div class="form-row">
          <div>
            <img id="perfil_imagem_path" src="<?=base_url().$dados["usuarioFoto"] ?>" style="width: 150px;height: 150px;">
            <label class="btn btn-block btn-info">
              <i class="fa fa-edit"></i>&nbsp;&nbsp;Alterar Foto
              <input type="file" id="btn_upload_imagem_perfil" accept="image/*" style="display: none;">
            </label>
            <input type="text" id="usuarioFoto" name="usuarioFoto" hidden>
            <span class="help-block"></span>
          </div>
        </div><br>
        <div class="form-row">
          <div class="   col-md-8 ">
            <label> Nome:</label>
            <div class="iconInput">
              <i class="fa fa-user"></i>
              <input type="text" name="usuarioNome" id="usuarioNome" class="form-control edit" value="<?= $dados["usuarioNome"] ?>" required>
              <span class="help-block"></span>
            </div>
          </div>
        </div>
        <br>
        <div class="form-row">
          <div class="   col-md-8">
            <label> Email:</label>
            <div class="iconInput">
              <i class="fa fa-envelope-square"></i>
              <input type="text" name="usuarioEmail" id="usuarioEmail" class="form-control edit" value="<?= $dados["usuarioEmail"] ?>" disabled>
            </div>
          </div>
        </div>
        <br>

        <div class="form-row">
          <div class="form-group col-md-6 ">
            <label for="inputCity">Rua:</label>
            <div class="iconInput">
              <i class="fa fa-map-marker-alt"></i>
              <input type="text" name="usuarioRua" id="usuarioRua" class="form-control edit" value="<?= $dados["usuarioRua"] ?>" required>
            </div>
          </div>
          <div class="form-group col-md-2">
            <label for="inputCity">Número:</label>
            <div class="iconInput">
              <i class="fa fa-sort-numeric-up"></i>
              <input type="text" name="usuarioNumero" id="usuarioNumero" class="form-control edit" value="<?= $dados["usuarioNumero"] ?>" maxlength="5" required>
            </div>
          </div>
        </div>

        <div class="form-row">
          <div class="col-md-4 ">
            <label> Bairro:</label>
            <div class="iconInput">
              <i class="fa fa-map-marker-alt"></i>
              <input type="text" name="usuarioBairro" id="usuarioBairro" class="form-control edit" value="<?= $dados["usuarioBairro"] ?>" required>
            </div>
          </div>
        </div>
        <br>
        <div class="form-row">
          <div class="form-group col-md-4 ">
            <label for="inputCity">Cidade:</label>
            <div class="iconInput">
              <i class="fa fa-map-marker-alt"></i>
              <input type="text" name="usuarioCidade" id="usuarioCidade" class="form-control edit" value="<?= $dados["usuarioCidade"] ?>" required>
            </div>
          </div>
          <div class="form-group col-md-3">
            <label for="inputCity">Estado:</label>
            <select id="usuarioEstado" name="usuarioEstado" class="form-control edit" required>

              <option value="AC" <?= ($dados["usuarioEstado"] == 'AC') ? 'selected' : '' ?>>Acre</option>
              <option value="AL" <?= ($dados["usuarioEstado"] == 'AL') ? 'selected' : '' ?>>Alagoas</option>
              <option value="AP" <?= ($dados["usuarioEstado"] == 'AP') ? 'selected' : '' ?>>Amapá</option>
              <option value="AM" <?= ($dados["usuarioEstado"] == 'AM') ? 'selected' : '' ?>>Amazonas</option>
              <option value="BA" <?= ($dados["usuarioEstado"] == 'BA') ? 'selected' : '' ?>>Bahia</option>
              <option value="CE" <?= ($dados["usuarioEstado"] == 'CE') ? 'selected' : '' ?>>Ceará</option>
              <option value="DF" <?= ($dados["usuarioEstado"] == 'DF') ? 'selected' : '' ?>>Distrito Federal</option>
              <option value="ES" <?= ($dados["usuarioEstado"] == 'ES') ? 'selected' : '' ?>>Espírito Santo</option>
              <option value="GO" <?= ($dados["usuarioEstado"] == 'GO') ? 'selected' : '' ?>>Goiás</option>
              <option value="MA" <?= ($dados["usuarioEstado"] == 'MA') ? 'selected' : '' ?>>Maranhão</option>
              <option value="MT" <?= ($dados["usuarioEstado"] == 'MT') ? 'selected' : '' ?>>Mato Grosso</option>
              <option value="MS" <?= ($dados["usuarioEstado"] == 'MS') ? 'selected' : '' ?>>Mato Grosso do Sul</option>
              <option value="MG" <?= ($dados["usuarioEstado"] == 'MG') ? 'selected' : '' ?>>Minas Gerais</option>
              <option value="PA" <?= ($dados["usuarioEstado"] == 'PA') ? 'selected' : '' ?>>Pará</option>
              <option value="PB" <?= ($dados["usuarioEstado"] == 'PB') ? 'selected' : '' ?>>Paraíba</option>
              <option value="PR" <?= ($dados["usuarioEstado"] == 'PR') ? 'selected' : '' ?>>Paraná</option>
              <option value="PE" <?= ($dados["usuarioEstado"] == 'PE') ? 'selected' : '' ?>>Pernambuco</option>
              <option value="PI" <?= ($dados["usuarioEstado"] == 'PI') ? 'selected' : '' ?>>Piauí</option>
              <option value="RJ" <?= ($dados["usuarioEstado"] == 'RJ') ? 'selected' : '' ?>>Rio de Janeiro</option>
              <option value="RN" <?= ($dados["usuarioEstado"] == 'RN') ? 'selected' : '' ?>>Rio Grande do Norte</option>
              <option value="RS" <?= ($dados["usuarioEstado"] == 'RS') ? 'selected' : '' ?>>Rio Grande do Sul</option>
              <option value="RO" <?= ($dados["usuarioEstado"] == 'RO') ? 'selected' : '' ?>>Rondônia</option>
              <option value="RR" <?= ($dados["usuarioEstado"] == 'RR') ? 'selected' : '' ?>>Roraima</option>
              <option value="SC" <?= ($dados["usuarioEstado"] == 'SC') ? 'selected' : '' ?>>Santa Catarina</option>
              <option value="SP" <?= ($dados["usuarioEstado"] == 'SP') ? 'selected' : '' ?>>São Paulo</option>
              <option value="SE" <?= ($dados["usuarioEstado"] == 'SE') ? 'selected' : '' ?>>Sergipe</option>
              <option value="TO" <?= ($dados["usuarioEstado"] == 'TO') ? 'selected' : '' ?>>Tocantins</option>
              <option value="EX" <?= ($dados["usuarioEstado"] == 'EX') ? 'selected' : '' ?>>Estrangeiro</option>

            </select>
          </div>
        </div>

        <br>
        <div class="form-row ">
          <div>
            <button type="submit" id="btn_editar" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar</button>
            <span class="help-block"></span>
          </div>
        </div>


      </form><br>

      <div class="form-row ">
        <div>
          <button class="btn btn-primary" id="altera_senha"><i class="fa fa-key"></i> Alterar Senha</button>
          <span class="help-block"></span>
        </div>
      </div>
    </div>
  </main>
</div>


<div class="modal" tabindex="-1" role="dialog" id="modal_altera_senha">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Alterar Senha</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="" method="post" id="formulario_altera_senha" name="formulario_altera_senha">
          <div class="form-row">
            <div class="form-group col-md-8 ">
              <label for="inputCity">Senha antiga :</label>
              <div class="iconInput">
                <i class="fa fa-key"></i>
                <input type="password" name="usuarioSenhaAntiga" id="usuarioSenhaAntiga" class="form-control" placeholder="Digite sua senha antiga">

              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-8 ">
              <label for="inputCity">Nova Senha :</label>
              <div class="iconInput">
                <i class="fa fa-key"></i>
                <input type="password" name="usuarioSenha" id="usuarioSenha" class="form-control" placeholder="Digite sua nova senha">
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-8 ">
              <label for="inputCity">Confirmar Senha :</label>
              <div class="iconInput">
                <i class="fa fa-key"></i>
                <input type="password" name="usuarioConfirmaSenha" id="usuarioConfirmaSenha" class="form-control" placeholder="Confirme sua nova senha">
              </div>
            </div>
          </div>
          <div class="form-row ">
            <div>
              <button type="submit" id="btn_salva_senha" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar</button>
              <span class="help-block"></span>
            </div>
          </div>

        </form>
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>