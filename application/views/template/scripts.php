 <script type="text/javascript" src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
 <script type="text/javascript" src="<?php echo base_url(); ?>public/js/base.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/script.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<?php  if(isset($scripts)) {
  foreach ($scripts as $script_name) {
    $src = base_url() . "public/js/" . $script_name; ?>
    <script src="<?=$src?>"></script>
  <?php }
} ?>  
</body>
</html>