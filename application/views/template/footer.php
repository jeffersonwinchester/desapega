 <!-- Footer -->
 <div class="col-12 align-self-end">
                <footer class="row">
                    <div class="col-12 bg-dark text-white pb-3 pt-5">
                        <div class="row">
                            <div class="col-lg-2 col-sm-4 text-center text-sm-left mb-sm-0 mb-3">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="footer-logo">
                                            <a href="index.html">Desapega Capelinha</a>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <address>
                                            Capelinha, MG
                                        </address>
                                    </div>
                                    <div class="col-12">
                                        <a href="#" class="social-icon"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#" class="social-icon"><i class="fab fa-instagram"></i></a>
                                        <a href="#" class="social-icon"><i class="fa fa-envelope"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-8 text-center text-sm-left mb-sm-0 mb-3">
                                <div class="row">
                                    <div class="col-12 text-uppercase">
                                        <h4>Quem Somos?</h4>
                                    </div>
                                    <div class="col-12 text-justify">
                                        <p>Somos um anunciente de produtos novos e usados vendidos em Capelinha MG. Não intermediamos as negociações ou realizamos transações financeiras.
                                        </p>
                                        <p>Negócie diretamente com o vendedor forma de pagamento bem como como a entrega diretamente pelo chat.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-sm-3 col-5 ml-lg-auto ml-sm-0 ml-auto mb-sm-0 mb-3">
                                <div class="row">
                                    <div class="col-12 text-uppercase">
                                        <h4>Links Utéis</h4>
                                    </div>
                                    <div class="col-12">
                                        <ul class="footer-nav">
                                            <li>
                                                <a href="#">Ínicio</a>
                                            </li>
                                            <li>
                                                <a href="#">Contate-nos</a>
                                            </li>
                                            <li>
                                                <a href="#">Política e Privacidade</a>
                                            </li>
        
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1 col-sm-2 col-4 mr-auto mb-sm-0 mb-3">
                                <div class="row">
                                    <div class="col-12 text-uppercase text-underline">
                                        <h4>Ajuda</h4>
                                    </div>
                                    <div class="col-12">
                                        <ul class="footer-nav">
                                            <li>
                                                <a href="#">Como Anunciar?</a>
                                            </li>
                                            <li>
                                                <a href="#">Reportar Vendedor</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- Footer -->
            </div>
        </div>
    </div>

    