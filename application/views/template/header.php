<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $titulo ?></title>



    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/fontawesome-free/css/all.min.css">
   
    
   

    <?php if (isset($styles)) {

        foreach ($styles as $style_name) {
            $href = base_url() . "public/css/" . $style_name; ?>
            <link href="<?= $href ?>" rel="stylesheet">

    <?php }
    }
    ?>
</head>

<body>
    <div class="container-fluid">

        <div class="row min-vh-100">
            <div class="col-12">
                <header class="row">
                    <!-- Top Nav -->
                    <div class="col-12 bg-dark py-2 d-md-block ">
                        <div class="row">
                            <div class="col-auto mr-auto">
                                <ul class="top-nav">

                                </ul>
                            </div>
                            <div class="col-auto ">
                                <ul class="top-nav">
                                    <?php

                                    if (!isset($usuario)) {
                                    ?>
                                        <li>
                                            <a href="<?php echo base_url(); ?>cadastro"><i class="fas fa-user-edit mr-2"></i>Cadastre-se</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url(); ?>login"><i class="fas fa-sign-in-alt mr-2"></i>Login</a>
                                        </li>
                                    <?php } else {
                                    ?>

                                        <li>
                                            <a href="<?php echo base_url(); ?>usuario"><i class="fas fa-user mr-2"></i>Meus Dados</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url(); ?>meusprodutos"><i class="fas fa-suitcase mr-2"></i>Vender</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url(); ?>login/logoff"><i class="fa fa-power-off mr-2"></i>Sair</a>
                                        </li>

                                    <?php }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Top Nav -->

                    <!-- Header -->
                    <div class="col-12 bg-white pt-4">
                        <div class="row">
                            <div class="col-lg-auto">
                                <div class="site-logo text-center text-lg-left">
                                    <a href="<?php echo base_url(); ?>inicio"><img src="<?php echo base_url(); ?>public/img/logodesapega.png"></a>
                                </div>
                            </div>
                            <div class="col-lg-5 mx-auto mt-4 mt-lg-0">
                                <form action="<?php echo base_url(); ?>produtos" method="GET">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="search" class="form-control border-dark" name="produto" placeholder="Buscar..." required>
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-dark"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <?php

                            if (isset($usuario)) {
                            ?>
                                <div class="col-lg-auto text-center text-lg-left header-item-holder">
                                    <a href="<?php echo base_url(); ?>chat" class="header-item">
                                        <i class="fas fa-comments"></i><span id="header-favorite">0</span>
                                    </a>

                                    <a href="<?php echo base_url(); ?>favoritos" class="header-item" id="favorito_atualiza">
                                        <i class="fas fa-heart mr-2"></i><span id="header-qty" class="mr-3"><?= $numerofavoritos ?></span>
                                    </a>
                                </div>
                        </div>
                    <?php }
                    ?>

                    <!-- Nav -->
                    <div class="row">
                        <nav class="navbar navbar-expand-lg navbar-light bg-white col-12">
                            <button class="navbar-toggler d-lg-none border-0" type="button" data-toggle="collapse" data-target="#mainNav">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="mainNav">
                                <ul class="navbar-nav mx-auto mt-2 mt-lg-0" >
                                    <li class="nav-item active" >
                                        <a class="nav-link" href="<?php echo base_url(); ?>inicio">Ínicio <span class="sr-only">(current)</span></a>
                                    </li>
                                    <?php
                                    foreach ($menu as $itens) {
                                        echo $itens;
                                    }

                                    ?>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <!-- Nav -->

                    </div>
                    <!-- Header -->

                </header>
            </div>