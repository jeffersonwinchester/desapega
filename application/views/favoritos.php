<div class="col-12">
                <!-- Main Content -->
                <div class="row">
                    <div class="col-12 mt-3 text-center text-uppercase">
                        <h2>Produtos Favoritos</h2>
                    </div>
                </div>

                <main class="row">
                    <div class="col-12 bg-white py-3 mb-3">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 col-sm-10 mx-auto table-responsive">
                                    <div class="col-12">
                                        <table class="table table-striped table-hover table-sm" id="tabela_favoritos">
                                            <thead>
                                               
                                            <tr>
                                                <th>Produto</th>
                                                <th>Valor</th>
                                                <th>Remover</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                    if (!empty($listagemfavoritos)) {
                                                        foreach ($listagemfavoritos as $favorito) {
                                                            if (empty($favorito['produtoValorPromocao'])) {
                                                                $valor = $favorito['produtoValor'];
                                                            }
                                                            else
                                                            {
                                                                $valor = $favorito['produtoValorPromocao'];
                                                            }
                                                           echo '
                                                           <tr>
                                                           <td>
                                                               <a href="#" style="text-decoration: none;color: black;"><img src="'.base_url().$favorito['produtoImagemPrincipal'].'" class="img-fluid img-fav">
                                                               '.$favorito['produtoNome'].'</a>
                                                           </td>
                                                           <td>
                                                           R$'.$valor.'
                                                           </td>
                                                           <td>
                                                               <button class="btn btn-link text-danger remover_favorito" produto='.$favorito['produtoId'].' chave='.$favorito['produtoHash'].' favorito='.$favorito['favoritoId'].'><i class="fas fa-trash-alt mr-2"></i></button>
                                                           </td>
                                                       </tr>
                                                           ';
                                                        }
                                                    }
                                                    else
                                                    {
                                                        echo '
                                                        <tr>
                                                            <td colspan="3" class="text-center">Nenhum produto adicionado</td>
                                                        </tr>
                                                        ';
                                                    }
                                                ?>
                                           
                                            
                                            </tbody>
                                            
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>

                </main>
                <!-- Main Content -->
            </div>