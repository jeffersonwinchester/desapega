 <div class="col-12">
     <!-- Main Content -->
     <main class="row">

         <!-- Category Products -->
         <div class="col-12">
             <div class="row">
                 <div class="col-12 py-3">
                     <div class="row">
                         <div class="col-12 text-center text-uppercase">
                             <h2><?= $categorianome ?></h2>
                         </div>
                     </div>
                     <div class="row">
                         <?php
                            if (empty($produtos)) {
                                echo '
                                <div class="col-12 text-center text-uppercase text-danger mt-5">
                             <h2>Nenhum produto encontrado!</h2>
                         </div>
                                ';
                            } else {
                                foreach ($produtos as $produto) {
                                    if (!empty($produto['produtoValorPromocao'])) {
                                        $promocao = '
                                        <span class="product-price-old">
                                                        ' . $produto['produtoValor'] . '
                                                    </span>
                                                <br>
                                        <span class="product-price">
                                        R$'.$produto['produtoValorPromocao'].'
                                            </span>
                                        ';
                                    }
                                    else
                                    {
                                        $promocao = '
                                        <span class="product-price">
                                        R$'.$produto['produtoValor'].'
                                            </span>
                                        ';
                                    }
                                    $titulo_novo = strtolower(preg_replace(
                                        "/[ -]+/",
                                        "-",
                                        strtr(
                                            utf8_decode(trim($produto['produtoNome'])),
                                            utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),
                                            "aaaaeeiooouuncAAAAEEIOOOUUNC-"
                                        )
                                    ));
                                    echo '
                                        <!-- Product -->
                                <div class="col-xl-2 col-lg-3 col-sm-6 my-3">
                                    <div class="col-12 bg-white text-center h-100 product-item">
                                        <div class="row h-100">
                                            <div class="col-12 p-0 mb-3">
                                            <a href="'.base_url().'produto?produto='.$titulo_novo.'&codigo='.$produto['produtoId'].'">
                                                    <img src="' . base_url() .$produto['produtoImagemPrincipal'].'" class="img-fluid img-cat">
                                                </a>
                                            </div>
                                            <div class="col-12 mb-3">
                                                <a href="'.base_url().'produto?produto='.$titulo_novo.'&codigo='.$produto['produtoId'].'" class="product-name">' . $produto['produtoNome'] . '</a>
                                            </div>
                                            <div class="col-12 mb-3">
                                                '.$promocao.'
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Product -->
                                        
                                        ';
                                }
                            }
                            ?>




                     </div>
                 </div>
             </div>
         </div>
         <!-- Category Products -->

         <div class="col-12">
             <nav aria-label="Page navigation example">
                 <ul class="pagination justify-content-center">
                     <li class="page-item disabled">
                         <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fas fa-long-arrow-alt-left"></i></a>
                     </li>
                     <li class="page-item"><a class="page-link" href="#">1</a></li>
                     <li class="page-item active" aria-current="page">
                         <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                     </li>
                     <li class="page-item"><a class="page-link" href="#">3</a></li>
                     <li class="page-item">
                         <a class="page-link" href="#"><i class="fas fa-long-arrow-alt-right"></i></a>
                     </li>
                 </ul>
             </nav>
         </div>

     </main>
     <!-- Main Content -->
 </div>