<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Alteração de Senha</title>



    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/fontawesome-free/css/all.min.css">
</head>

<body>
    <div class="container-fluid">
        <div class="row min-vh-100">
            <div class="col-12">
                <header class="row">
                    <!-- Top Nav -->
                    <div class="col-12 bg-dark py-2 d-md-block d-none">
                        <div class="row">
                            <div class="col-auto mr-auto">

                            </div>
                            <div class="col-auto">
                                <ul class="top-nav">
                                    <li>
                                        <a href="<?php echo base_url(); ?>cadastro"><i class="fas fa-user-edit mr-2"></i>Cadastre-se</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>login"><i class="fas fa-sign-in-alt mr-2"></i>Login</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Top Nav -->

                    <!-- Header -->
                    <div class="col-12 bg-white pt-4">
                        <div class="row">
                            <div class="col-lg-auto">
                                <div class="site-logo text-center text-lg-left">
                                    <a href="<?php echo base_url(); ?>inicio"><img src="<?php echo base_url(); ?>public/img/logodesapega.png"></a>
                                </div>
                            </div>

                        </div>

                        <!-- Nav -->
                        <div class="row">
                            <nav class="navbar navbar-expand-lg navbar-light bg-white col-12">
                                <button class="navbar-toggler d-lg-none border-0" type="button" data-toggle="collapse" data-target="#mainNav">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="mainNav">
                                    <ul class="navbar-nav mx-auto mt-2 mt-lg-0">
                                        <li class="nav-item active">
                                            <a class="nav-link" href="<?php echo base_url(); ?>inicio">Ínicio <span class="sr-only">(current)</span></a>
                                        </li>
                                        <?php
                                        foreach ($menu as $itens) {
                                            echo $itens;
                                        }

                                        ?>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                        <!-- Nav -->

                    </div>
                    <!-- Header -->

                </header>
            </div>

            <div class="col-12">
                <!-- Main Content -->
                <div class="row">
                    <div class="col-12 mt-3 text-center text-uppercase">
                        <h2>Alteração de Senha</h2>
                    </div>
                </div>

                <main class="row">
                    <?php
                    if ($url == 1) {
                    ?>
                        <div class="col-lg-4 col-md-6 col-sm-8 mx-auto bg-white py-3 mb-4">
                            <div class="row">
                                <div class="col-12">


                                    <form method="POST" action="" name="form_esqueceu_senha" id="form_esqueceu_senha">
                                        <div class="form-group">
                                            <label for="novaSenha">Nova Senha</label>
                                            <input type="password" id="novaSenha" name="novaSenha" class="form-control" required minlength="6">
                                        </div>
                                        <div class="form-group">
                                            <label for="confirmSenha">Confirmar Senha</label>
                                            <input type="password" name="confirmSenha" id="confirmSenha" class="form-control" required minlength="6">
                                        </div>
                                        <div id="info">
                                            <div class="form-group">
                                                <input type="text" name="hash" id="hash" class="form-control" value="<?= $_GET['hash'] ?>" hidden>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="email" id="email" class="form-control" value="<?= $_GET['email'] ?>" hidden>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" id="btn-esquece_senha" class="btn btn-outline-dark">Alterar</button>
                                        </div>

                                    </form>

                                </div>
                            </div>
                        </div>
                    <?php } else {
                    ?>
                        <div class="col-lg-4 col-md-6 col-sm-8 mx-auto py-3 mb-4">
                            <div class="row">
                                <div class="col-12">
                                    <h1 class="text-danger text-center">Url Inválida</h1>
                                </div>
                            </div>
                        </div>
                    <?php }
                    ?>

                </main>
                <!-- Main Content -->
            </div>

            <div class="col-12 align-self-end">
                <!-- Footer -->
                <footer class="row">
                    <div class="col-12 bg-dark text-white pb-3 pt-5">
                        <div class="row">
                            <div class="col-lg-2 col-sm-4 text-center text-sm-left mb-sm-0 mb-3">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="footer-logo">
                                            <a href="index.html">Desapega Capelinha</a>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <address>
                                            Capelinha, MG
                                        </address>
                                    </div>
                                    <div class="col-12">
                                        <a href="#" class="social-icon"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#" class="social-icon"><i class="fab fa-instagram"></i></a>
                                        <a href="#" class="social-icon"><i class="fa fa-envelope"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-8 text-center text-sm-left mb-sm-0 mb-3">
                                <div class="row">
                                    <div class="col-12 text-uppercase">
                                        <h4>Quem Somos?</h4>
                                    </div>
                                    <div class="col-12 text-justify">
                                        <p>Somos um anunciente de produtos novos e usados vendidos em Capelinha MG. Não intermediamos as negociações ou realizamos transações financeiras.
                                        </p>
                                        <p>Negócie diretamente com o vendedor forma de pagamento bem como como a entrega diretamente pelo chat.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-sm-3 col-5 ml-lg-auto ml-sm-0 ml-auto mb-sm-0 mb-3">
                                <div class="row">
                                    <div class="col-12 text-uppercase">
                                        <h4>Links Utéis</h4>
                                    </div>
                                    <div class="col-12">
                                        <ul class="footer-nav">
                                            <li>
                                                <a href="#">Ínicio</a>
                                            </li>
                                            <li>
                                                <a href="#">Contate-nos</a>
                                            </li>
                                            <li>
                                                <a href="#">Política e Privacidade</a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1 col-sm-2 col-4 mr-auto mb-sm-0 mb-3">
                                <div class="row">
                                    <div class="col-12 text-uppercase text-underline">
                                        <h4>Ajuda</h4>
                                    </div>
                                    <div class="col-12">
                                        <ul class="footer-nav">
                                            <li>
                                                <a href="#">Como Anunciar?</a>
                                            </li>
                                            <li>
                                                <a href="#">Reportar Vendedor</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </footer>
                <!-- Footer -->
            </div>

        </div>

        <script type="text/javascript" src="<?php echo base_url(); ?>public/js/base.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/js/script.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/js/alterasenha.js"></script>
</body>

</html>