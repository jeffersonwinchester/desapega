<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>

   <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
   <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   <meta name="format-detection" content="date=no" />
   <meta name="format-detection" content="address=no" />
   <meta name="format-detection" content="telephone=no" />
   <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet" />
   <title>Email</title>


   <style type="text/css" media="screen">
      [style*="Roboto"] { 
         font-family: 'Roboto', Arial, sans-serif !important
      }

      .text-top a { color:#26252a; text-decoration:none }

      .text-footer a,
      .text-footer-r a { color:#1f1e23; text-decoration:none }

      .text-date2 a { color:#f6de17; text-decoration:none }

      .text-top-white a,
      .text2-center-white a { color:#ffffff; text-decoration:none }

      /* Linked Styles */
      body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#1f1e23; -webkit-text-size-adjust:none }
      a { color:#26252a; text-decoration:none }
      p { padding:0 !important; margin:0 !important } 
   img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }

   /* Mobile styles */
   @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
      table[class='mobile-shell'] { width: 100% !important; min-width: 100% !important; }
      table[class='center'] { margin: 0 auto !important; }

      td[class='td'] { width: 100% !important; min-width: 100% !important; }

      div[class='mobile-br-5'] { height: 5px !important; }
      div[class='mobile-br-10'] { height: 10px !important; }
      div[class='mobile-br-15'] { height: 15px !important; }
      div[class='mobile-br-25'] { height: 25px !important; }

      th[class='m-td'],
      td[class='m-td'],
      table[class='m-td'],
      div[class='hide-for-mobile'], 
      span[class='hide-for-mobile'] { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }
      td[class='m-auto'] { width: auto !important; }

      span[class='mobile-block'] { display: block !important; }
      td[class='text-top'],
      td[class='text-top-r'],
      td[class='text-top-grey'],
      td[class='text-top-white'],
      div[class='text-1'],
      div[class='text-footer'],
      div[class='text-footer-r'],
      div[class='footer-title'],
      div[class='img-m-center'] { text-align: center !important; }

      div[class='img-m-left'] { text-align: left !important; }

      div[class='fluid-img'] img,
      td[class='fluid-img'] img { width: 100% !important; max-width: 100% !important; height: auto !important; }
      td[class='fluid-img2'] { width: auto !important; }
      td[class='fluid-img2'] img { width: 100% !important; max-width: 100% !important; height: auto !important; }

      th[class='column'],
      th[class='column-top'],
      th[class='column-bottom'],
      th[class='column-dir'] { float: left !important; width: 100% !important; display: block !important; }

      td[class='content-spacing'] { width: 10px !important; }

      td[class='text-2-c'] { width: 20px !important; }
      div[class='text-footer-c'] { font-size: 11px !important; line-height: 20px !important; }
   }
</style>
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#1f1e23; -webkit-text-size-adjust:none">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#1f1e23">
      <tr>
         <td align="center" valign="top">
            <!-- Header -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#1f1e23">
               <tr>
                  <td align="center">
                     <div class="hide-for-mobile"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="40" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>
                     </div>
                     <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell" bgcolor="#f4f4f4">
                        <tr>
                           <td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                              <div class="hide-for-mobile"><img src="<?php echo base_url(); ?>public/img/email/black_white_top_image.jpg" border="0" width="650" height="30" alt="" /></div>
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                 <tr>
                                    <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                                    <td>
                                       <div style="font-size:0pt; line-height:0pt;" class="mobile-br-15"></div>

                                       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                             <th class="column-top" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0" width="200">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                   <tr>
                                                      <td class="text-top" style="color:#26252a; font-family:Arial,sans-serif, 'Roboto'; font-size:11px; line-height:16px; text-align:left; text-transform:uppercase; min-width:auto !important">
                                                         Bem Vindo!
                                                         <div style="font-size:0pt; line-height:0pt;" class="mobile-br-10"></div>

                                                      </td>
                                                   </tr>
                                                </table>
                                             </th>
                                             <th class="column-top" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                   <tr>
                                                      <td class="text-top-r" style="color:#26252a; font-family:Arial,sans-serif, 'Roboto'; font-size:11px; line-height:16px; text-align:right; text-transform:uppercase; min-width:auto !important">
                                                         <a href="<?php echo base_url();?>" target="_blank" class="link-black-u" style="color:#26252a; text-decoration:underline"><span class="link-black-u" style="color:#26252a; text-decoration:underline">Visite o site</span></a> &nbsp; &nbsp; 
                                                      </td>
                                                   </tr>
                                                </table>
                                             </th>
                                          </tr>
                                       </table>
                                       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="15" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                       <div style="font-size:0pt; line-height:0pt;" class="mobile-br-10"></div>

                                    </td>
                                    <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#DC3545">
               <tr>
                  <td align="center">
                     <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                        <tr>
                           <td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                 <tr>
                                    <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                                    <td>
                                       <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="36" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                             <th class="column" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0" width="227">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                   <tr>
                                                      <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><div class="img-m-center" style="font-size:0pt; line-height:0pt"><img src="http://desapega.4asistemas.com/public/img/logodesapega.png" border="0" width="160" height="60" alt="MailBakery Iota - Email Newsletter Template" /></div><div style="font-size:0pt; line-height:0pt;" class="mobile-br-15"></div>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </th>
                                             <th class="column" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                   <tr>
                                                      <td align="right">
                                                         <table class="center" border="0" cellspacing="0" cellpadding="0">
                                                         <!-- <?php 
                                                         /*
                                                           if ($dados_empresa->empresa_whatsapp == "") {
                                                           $dados_empresa->empresa_whatsapp = "href=#";
                                                        }
                                                        else
                                                        {
                                                         $dados_empresa->empresa_whatsapp = 'href=https://api.whatsapp.com/send?phone='.$dados_empresa->empresa_whatsapp.'&text=Ol%C3%A1%2C%20visitei%20o%20seu%20site%20e%20estou%20com%20d%C3%BAvida. target=_blank';

                                                      }
                                                      if ($dados_empresa->empresa_facebook == "") {
                                                      $dados_empresa->empresa_facebook = "href=#";
                                                   }
                                                   else
                                                   {
                                                      $dados_empresa->empresa_facebook = 'href='.$dados_empresa->empresa_facebook.' target=_blank';
                                                   }
                                                   if ($dados_empresa->empresa_instagram == "") {
                                                   $dados_empresa->empresa_instagram = "href=#";
                                                }
                                                else
                                                {
                                                   $dados_empresa->empresa_instagram = 'href='.$dados_empresa->empresa_instagram.' target=_blank';
                                                }*/

                                                ?>-->

                                                <tr>
                                                   <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#" ><img src="<?php echo base_url(); ?>public/img/icones/whats.png" border="0" width="24" height="23" alt="" /></a></td>
                                                   <td class="text-2-c" style="color:#1f1e23; font-family:Arial,sans-serif, 'Roboto'; font-size:12px; line-height:16px; text-align:center; min-width:auto !important" width="27">/</td>
                                                   <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#"><img src="<?php echo base_url(); ?>public/img/icones/face.png" border="0" width="24" height="23" alt="" /></a></td>
                                                   <td class="text-2-c" style="color:#1f1e23; font-family:Arial,sans-serif, 'Roboto'; font-size:12px; line-height:16px; text-align:center; min-width:auto !important" width="27">/</td>



                                                   <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#"><img src="<?php echo base_url(); ?>public/img/icones/insta.png" border="0" width="24" height="23" alt="" /></a></td>
                                                </tr>
                                             </table>
                                          </td>
                                       </tr>
                                    </table>
                                 </th>
                              </tr>
                           </table>
                           <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                        </td>
                        <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                     </tr>
                  </table>
               </td>
            </tr>
         </table>
      </td>
   </tr>
</table>
<!-- END Header -->

<!-- Section 1 -->
<div mc:repeatable="Select" mc:variant="Section 1">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
      <tr>
         <td align="center">
            <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell" bgcolor="white">
               <tr>
                  <td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">
                     <!-- Full Width Image -->
                     <div class="fluid-img" style="font-size:0pt; line-height:0pt; text-align:left"></div>
                     <!-- END Full Width Image -->

                     <!-- Content -->
                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                           <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                           <td>
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                              <div class="h2-black-center" style="color:#26252a; font-family:Arial,sans-serif, 'Roboto'; font-size:26px; line-height:32px; text-align:center; min-width:auto !important"><strong><?= $assunto ?></strong></div>
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="25" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                              <div class="text-center" style="color:#26252a; font-family:Arial,sans-serif, 'Roboto'; font-size:14px; line-height:28px; text-align:center; min-width:auto !important">

                              <?= $mensagem?>
                              </div>
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="22" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


                              <!-- Button -->
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                 <tr>
                                    <td align="center">
                                       <table bgcolor="#2e2d33" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                             <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="22"></td>

                                             <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="22"></td>
                                          </tr>
                                       </table>
                                    </td>
                                 </tr>
                              </table>
                              <!-- END Button -->
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="12" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                           </td>
                           <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                        </tr>
                     </table>
                     <!-- Content -->
                  </td>
               </tr>
            </table>

         </td>
      </tr>
   </table>
</div>
<!-- END Section 1 -->









<!-- Footer -->
<div mc:repeatable="Select" mc:variant="Footer">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
      <tr>
         <td align="center">
            <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
               <tr>
                  <td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0" align="center">
                     <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="40" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                     <!-- Logo Center -->
                     <div class="img-center" style="font-size:0pt; line-height:0pt; text-align:center"><a href="<?php echo base_url(); ?>" target="_blank"><img src="http://desapega.4asistemas.com/public/img/logodesapega.png" border="0" width="174" height="120" alt="MailBakery Iota - Email Newsletter Template" /></a></div>
                     <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="25" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                     <!-- END Logo Center -->

                     <!-- Socials -->
                     <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                           <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#"><img src="<?php echo base_url(); ?>public/img/icones/whats.png" border="0" width="36" height="40" alt="rss" /></a></td>

                           <td class="text-2-c" style="color:#1f1e23; font-family:Arial,sans-serif, 'Roboto'; font-size:12px; line-height:16px; text-align:center; min-width:auto !important" width="27">/</td>

                           <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#"><img src="<?php echo base_url(); ?>public/img/icones/face.png" border="0" width="36" height="40" alt="rss" /></a></td>

                           <td class="text-2-c" style="color:#1f1e23; font-family:Arial,sans-serif, 'Roboto'; font-size:12px; line-height:16px; text-align:center; min-width:auto !important" width="27">/</td>




                           <td class="img" style="font-size:0pt; line-height:0pt; text-align:left"><a href="#"><img src="<?php echo base_url(); ?>public/img/icones/insta.png" border="0" width="36" height="40" alt="rss" /></a></td>
                        </tr>
                     </table>
                     <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="25" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                     <!-- END Socials -->
                     <div class="fluid-img" style="font-size:0pt; line-height:0pt; text-align:left"><img src="<?php echo base_url(); ?>public/img/email/white_grey_top_image.jpg" style="display: block;" border="0" width="650" height="55" alt="" /></div>
                  </td>
               </tr>
            </table>
         </td>
      </tr>
   </table>

   <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#1f1e23">
      <tr>
         <td align="center">
            <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
               <tr>
                  <td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0" align="center">
                     <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#373737">
                        <tr>
                           <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                           <td>
                              <div class="text2-center-white" style="color:#ffffff; font-family:Arial,sans-serif, 'Roboto'; font-size:12px; line-height:20px; text-align:center; min-width:auto !important">
                                 Esse e-mail é automático, não é necessário respondê-lo. 
                              </div>
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


                              <div class="text2-center-white" style="color:#ffffff; font-family:Arial,sans-serif, 'Roboto'; font-size:12px; line-height:20px; text-align:center; min-width:auto !important">
                                 Copyright &copy; 2020 4A SISTEMAS
                              </div>
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="15" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                              <div class="hide-for-mobile"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>
                              </div>
                           </td>
                           <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
         </td>
      </tr>
   </table>
   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="5" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

</div>
<div class="hide-for-mobile"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="40" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>
</div>
<!-- END Footer -->
</td>
</tr>
</table>
</body>
</html>
