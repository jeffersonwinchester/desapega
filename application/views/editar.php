<div class="container">
  <div class="row">
    <div class="col-12 mt-3 text-center text-uppercase">
      <h2>Edição de Produtos</h2>
    </div>
    
  </div>
  <?php

if (empty($listaproduto)) {
?>
  <div class="col-12 mt-3 text-center text-danger text-uppercase">
      <h2>Nenhum Produto Encontrado</h2>
    </div>
    <?php } else {
                                    ?>
  <form action="" method="POST" name="form_edita_produto" id="form_edita_produto" data-toggle="validator">

    <div class="form-row">
      <div class="   col-md-8 ">
        <label> Nome do Produto:</label>
        <div class="iconInput">
          <i class="fa fa-adn"></i>
          <input type="text" name="produtoNome" id="produtoNome" class="form-control " placeholder="Digite o nome do produto" value="<?= $listaproduto['produtoNome']?>">
          <span class="help-block"></span>
        </div>
      </div>
    </div>

    <br>
    <div class="form-row">
      <div class="form-group col-md-4 ">
        <label for="inputCity">Valor do Produto:</label>
        <div class="iconInput">
          <i class="fa fa-dollar-sign"></i>
          <input type="text" name="produtoValor" id="produtoValor" class="form-control" placeholder="Digite o valor do produto" value="<?= $listaproduto['produtoValor']?>">
        </div>
      </div>
    </div>
    <div class="form-row">
      <div class="col-md-8">
        <label> Produto está em promocão?</label>
        <div class="iconInput">
          
              <?php
                if (empty($listaproduto['produtoValorPromocao'])) {
                    echo '
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="produto_promocao" id="produto_promocao0" value="0" checked>
            <label class="form-check-label" for="inlineRadio1">Não</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="produto_promocao" id="produto_promocao1" value="1">
            <label class="form-check-label" for="inlineRadio2">Sim</label>
                    ';
                }
                else
                {
                    echo '
                    <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="produto_promocao" id="produto_promocao0" value="0" >
            <label class="form-check-label" for="inlineRadio1">Não</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="produto_promocao" id="produto_promocao1" value="1" checked>
            <label class="form-check-label" for="inlineRadio2">Sim</label>
                    ';
                }
              ?>
            
          </div>

        </div>
      </div>
    </div>
    <div id="promocao">
        <?php
    if (!empty($listaproduto['produtoValorPromocao'])) {
        echo '
        <br><div class="form-row">
            <div class="form-group col-md-4 ">
              <label for="inputCity">Valor do Produto na promoção:</label>
              <div class="iconInput">
                <i class="fa fa-dollar-sign"></i>
                <input type="text" name="produtoValorPromocao" id="produtoValorPromocao" class="form-control" placeholder="Digite o valor do produto na promoção" value='.$listaproduto['produtoValorPromocao'].'>
              </div>
            </div>
          </div>
        ';
    }
    ?>
    </div>
    <br>

    <div class="form-row">
      <div class="col-md-6">
        <label> Descricao:</label>
        <div class="iconInput">
          <textarea name="produtoDescricao" id="produtoDescricao" class="col-md-12"><?=$listaproduto['produtoDescricao']?></textarea>

        </div>
      </div>
    </div>
    <div class="form-row" style="margin-top: 2%;">
      <div class="form-group col-md-3 ">
        <label for="inputCity">Categoria:</label>
        <select id="produtoCategoria" name="produtoCategoria" class="form-control">
          <?php
          foreach ($categorias as $categoria) {
              if ($categoria["categoriaId"]==$subcategoria["categoriaId"]) {
                echo '
                <option value="'.$categoria['categoriaId'].'" selected>'.$categoria['categoriaNome'].'</option>
              ';
              }
              else
              {
                echo '
                <option value="'.$categoria['categoriaId'].'">'.$categoria['categoriaNome'].'</option>
              ';
              }
           
          }
          ?>

        </select>
      </div>
      <div class="form-group col-md-3">
        <label for="inputCity">Sub Categoria:</label>
        <select id="constroi" name="subCategoriaId" class="form-control">
          
        <?php
          foreach ($subcategorias as $sub) {
              if ($sub["subCategoriaId"]==$subcategoria["subCategoriaId"]) {
                echo '
                <option value="'.$sub['subCategoriaId'].'" selected>'.$sub['subCategoriaNome'].'</option>
              ';
              }
              else
              {
                echo '
                <option value="'.$sub['subCategoriaId'].'">'.$sub['subCategoriaNome'].'</option>
              ';
              }
           
          }
          ?>
        </select>
      </div>
    </div>

    <?php
    if (!empty($listaproduto['produtoImagemPrincipal'])) {
        $imagemprincipal = base_url().$listaproduto['produtoImagemPrincipal'];
    }
    else
    {
        $imagemprincipal = "";
    }
            if (!empty($listaproduto['produtoImagem1'])) {
                $imagem1 = base_url().$listaproduto['produtoImagem1'];
            }
            else
            {
                $imagem1 = "";
            }
            if (!empty($listaproduto['produtoImagem2'])) {
                $imagem2 = base_url().$listaproduto['produtoImagem2'];
            }
            else
            {
                $imagem2 = "";
            }
          ?>
    <div class="form-row">
      <div class="form-group col-md-3 ">
        <label for="inputCity">Imagem Principal:</label>
        <div>
          <img id="produto_imagem_path" src="<?=$imagemprincipal?>" style="max-width: 150px;max-height: 150px;">
          <label class="btn btn-block btn-info">
            <i class="fa fa-plus"></i>&nbsp;&nbsp;Imagem principal
            <input type="file" id="btn_upload_imagem_produto" accept="image/*" style="display: none;" produto="0">
          </label>
          <input type="text" id="produtoImagemPrincipal" name="produtoImagemPrincipal" hidden>
          <span class="help-block"></span>
        </div>
      </div>
    </div><br>
          
    <div class="form-row">
      <div class="form-group col-md-3 ">
        <label for="inputCity">Imagem 1:</label>
        <div>
          <img id="produto1_imagem_path" src="<?= $imagem1?>" style="max-width: 150px;max-height: 150px;">
          <label class="btn btn-block btn-info">
            <i class="fa fa-plus"></i>&nbsp;&nbsp;Imagem I
            <input type="file" id="btn_upload_imagem_produto1" accept="image/*" style="display: none;" produto="1">
          </label>
          <input type="text" id="produtoImagem1" name="produtoImagem1" hidden>
          <span class="help-block"></span>
        </div>
      </div>
      <div class="form-group col-md-3">
        <label for="inputCity">Imagem 2:</label>
        <div>
          <img id="produto2_imagem_path" src="<?=$imagem2?>" style="max-width: 150px;max-height: 150px;">
          <label class="btn btn-block btn-info">
            <i class="fa fa-plus"></i>&nbsp;&nbsp;Imagem II
            <input type="file" id="btn_upload_imagem_produto2" accept="image/*" style="display: none;" produto="2">
          </label>
          <input type="text" id="produtoImagem2" name="produtoImagem2" hidden>
          <span class="help-block"></span>
        </div>
      </div>
    </div>

    <br>
    <input type="text" name="chave" id="chave" class="form-control" value="<?= $_GET['chave']?>" hidden>
    <input type="text" name="produto" id="produto" class="form-control" value="<?= $_GET['produto']?>" hidden>
    <input type="text" name="codigo" id="codigo" class="form-control" value="<?= $_GET['codigo']?>" hidden>
    <div class="form-row ">
      <div>
        <button type="submit" id="btn_editar_produto" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar Alterações</button>
        <span class="help-block"></span>
      </div>
    </div><br>
    <div id="editando">
    </div>
    <br>


  </form>
  <?php }
                                    ?>

  <!-- /.content -->
</div>