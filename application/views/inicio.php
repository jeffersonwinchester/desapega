  <div class="col-12">
                <!-- Main Content -->
                <main class="row">

                    <!-- Slider -->
                    <div class="col-12 px-0">
                        <div id="slider" class="carousel slide w-100" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#slider" data-slide-to="0" class="active"></li>
                                <li data-target="#slider" data-slide-to="1"></li>
                                <li data-target="#slider" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <img src="<?php echo base_url(); ?>public/img/slider-1.jpg" class="slider-img">
                                </div>
                                <div class="carousel-item">
                                    <img src="<?php echo base_url(); ?>public/img/slider-2.jpg" class="slider-img">
                                </div>
                                <div class="carousel-item">
                                    <img src="<?php echo base_url(); ?>public/img/slider-3.jpg" class="slider-img">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <!-- Slider -->

                    <!-- Featured Products -->
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 py-3">
                                <div class="row">
                                    <div class="col-12 text-center text-uppercase">
                                        <h2>Top Seleção de Produtos</h2>
                                    </div>
                                </div>
                                <div class="row">
                                <?php
                                if (!empty($produtosselecionados)) {
                                    foreach ($produtosselecionados as $produto) {
                                        if (!empty($produto['produtoValorPromocao'])) {
                                            $promocao = '
                                            <span class="product-price-old">
                                                            ' . $produto['produtoValor'] . '
                                                        </span>
                                                    <br>
                                            <span class="product-price">
                                            R$'.$produto['produtoValorPromocao'].'
                                                </span>
                                            ';
                                        }
                                        else
                                        {
                                            $promocao = '
                                            <span class="product-price">
                                            R$'.$produto['produtoValor'].'
                                                </span>
                                            ';
                                        }
                                        $titulo_novo = strtolower(preg_replace(
                                            "/[ -]+/",
                                            "-",
                                            strtr(
                                                utf8_decode(trim($produto['produtoNome'])),
                                                utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),
                                                "aaaaeeiooouuncAAAAEEIOOOUUNC-"
                                            )
                                        ));
                                        echo '
                                        <div class="col-lg-3 col-sm-6 my-3">
                                        <div class="col-12 bg-white text-center h-100 product-item">
                                            <div class="row h-100">
                                                <div class="col-12 p-0 mb-3">
                                                    <a href="'.base_url().'produto?produto='.$titulo_novo.'&codigo='.$produto['produtoId'].'">
                                                        <img src="'.base_url().$produto['produtoImagemPrincipal'].'" class="img-fluid img-inicio">
                                                    </a>
                                                </div>
                                                <div class="col-12 mb-3">
                                                    <a href="'.base_url().'produto?produto='.$titulo_novo.'&codigo='.$produto['produtoId'].'" class="product-name">'.$produto['produtoNome'].'</a>
                                                </div>
                                                <div class="col-12 mb-3">
                                                    '.$promocao.'
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                        ';
                                     }
                                }
                                       


                                ?>
                                 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Featured Products -->

                    <div class="col-12">
                        <hr>
                    </div>

                    <!-- Latest Product -->
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12 py-3">
                                <div class="row">
                                    <div class="col-12 text-center text-uppercase">
                                        <h2>Últimos adicionados</h2>
                                    </div>
                                </div>
                                <div class="row">

                                <?php
                                    if (!empty($produtosnovos)) {
                                        foreach ($produtosnovos as $produtonovo) {
                                            if (!empty($produto['produtoValorPromocao'])) {
                                                $promocao = '
                                                <span class="product-price-old">
                                                                ' . $produtonovo['produtoValor'] . '
                                                            </span>
                                                        <br>
                                                <span class="product-price">
                                                R$'.$produtonovo['produtoValorPromocao'].'
                                                    </span>
                                                ';
                                            }
                                            else
                                            {
                                                $promocao = '
                                                <span class="product-price">
                                                R$'.$produtonovo['produtoValor'].'
                                                    </span>
                                                ';
                                            }
                                            $titulo_novo = strtolower(preg_replace(
                                                "/[ -]+/",
                                                "-",
                                                strtr(
                                                    utf8_decode(trim($produtonovo['produtoNome'])),
                                                    utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),
                                                    "aaaaeeiooouuncAAAAEEIOOOUUNC-"
                                                )
                                            ));
                                            echo '
                                            <div class="col-lg-3 col-sm-6 my-3">
                                            <div class="col-12 bg-white text-center h-100 product-item">
                                                <span class="new">Novo</span>
                                                <div class="row h-100">
                                                    <div class="col-12 p-0 mb-3">
                                                        <a href="'.base_url().'produto?produto='.$titulo_novo.'&codigo='.$produtonovo['produtoId'].'">
                                                            <img src="'.base_url().$produtonovo['produtoImagemPrincipal'].'" class="img-fluid img-inicio">
                                                        </a>
                                                    </div>
                                                    <div class="col-12 mb-3">
                                                        <a href="'.base_url().'produto?produto='.$titulo_novo.'&codigo='.$produtonovo['produtoId'].'" class="product-name">'.$produtonovo['produtoNome'].'</a>
                                                    </div>
                                                    <div class="col-12 mb-3">
                                                        '.$promocao.'
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                            '; 
                                        }
                                    }

                                ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Latest Products -->

                    <div class="col-12">
                        <hr>
                    </div>

                    

                    <div class="col-12 py-3 bg-light d-sm-block d-none">
                        <div class="row">
                            <div class="col-lg-3 col ml-auto large-holder">
                                <div class="row">
                                    <div class="col-auto ml-auto large-icon">
                                        <i class="fas fa-money-bill"></i>
                                    </div>
                                    <div class="col-auto mr-auto large-text">
                                        Melhores Preços
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col large-holder">
                                <div class="row">
                                    <div class="col-auto ml-auto large-icon">
                                        <i class="fas fa-handshake"></i>
                                    </div>
                                    <div class="col-auto mr-auto  large-text">
                                       Negócie com Vendedor
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col mr-auto large-holder">
                                <div class="row">
                                    <div class="col-auto ml-auto large-icon">
                                        <i class="fas fa-check"></i>
                                    </div>
                                    <div class="col-auto mr-auto large-text">
                                       Melhores Produtos
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <!-- Main Content -->
            </div>

            