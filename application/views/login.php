<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>E-Commerce Template</title>



    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/fontawesome-free/css/all.min.css">
</head>

<body>
    <div class="container-fluid">
        <div class="row min-vh-100">
            <div class="col-12">
                <header class="row">
                    <!-- Top Nav -->
                    <div class="col-12 bg-dark py-2 d-md-block d-none">
                        <div class="row">
                            <div class="col-auto mr-auto">

                            </div>
                            <div class="col-auto">
                                <ul class="top-nav">
                                    <li>
                                        <a href="<?php echo base_url(); ?>cadastro"><i class="fas fa-user-edit mr-2"></i>Cadastre-se</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>login"><i class="fas fa-sign-in-alt mr-2"></i>Login</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Top Nav -->

                    <!-- Header -->
                    <div class="col-12 bg-white pt-4">
                        <div class="row">
                            <div class="col-lg-auto">
                                <div class="site-logo text-center text-lg-left">
                                    <a href="<?php echo base_url(); ?>inicio"><img src="<?php echo base_url(); ?>public/img/logodesapega.png"></a>
                                </div>
                            </div>

                        </div>

                        <!-- Nav -->
                        <div class="row">
                            <nav class="navbar navbar-expand-lg navbar-light bg-white col-12">
                                <button class="navbar-toggler d-lg-none border-0" type="button" data-toggle="collapse" data-target="#mainNav">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="mainNav">
                                    <ul class="navbar-nav mx-auto mt-2 mt-lg-0">
                                        <li class="nav-item active">
                                            <a class="nav-link" href="<?php echo base_url(); ?>inicio">Ínicio <span class="sr-only">(current)</span></a>
                                        </li>
                                        <?php
                                        foreach ($menu as $itens) {
                                            echo $itens;
                                        }

                                        ?>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                        <!-- Nav -->

                    </div>
                    <!-- Header -->

                </header>
            </div>

            <div class="col-12">
                <!-- Main Content -->
                <div class="row">
                    <div class="col-12 mt-3 text-center text-uppercase">
                        <h2>Login</h2>
                    </div>
                </div>

                <main class="row">
                    <div class="col-lg-4 col-md-6 col-sm-8 mx-auto bg-white py-3 mb-4">
                        <div class="row">
                            <div class="col-12">
                                <form method="POST" action="" name="form_valida_login" id="form_valida_login">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" id="usuarioEmail" name="usuarioEmail" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Senha</label>
                                        <input type="password" name="usuarioSenha" id="usuarioSenha" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" id="btn-login" class="btn btn-outline-dark">Login</button>
                                    </div>
                                    <div class="d-flex flex-row p-2" id="logando">


                                    </div>
                                    <div class="text-center text-danger">
                                        <?php
                                        if (isset($mensagem)) {
                                            echo $mensagem;
                                        }

                                        ?>
                                    </div>
                                </form>
                                <a href="#" id="btn_solicita" style="text-decoration: none;">Esqueci minha senha?</a>
                            </div>
                        </div>
                    </div>

                </main>
                <!-- Main Content -->
            </div>

            <div class="col-12 align-self-end">
                <!-- Footer -->
                <footer class="row">
                    <div class="col-12 bg-dark text-white pb-3 pt-5">
                        <div class="row">
                            <div class="col-lg-2 col-sm-4 text-center text-sm-left mb-sm-0 mb-3">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="footer-logo">
                                            <a href="index.html">Desapega Capelinha</a>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <address>
                                            Capelinha, MG
                                        </address>
                                    </div>
                                    <div class="col-12">
                                        <a href="#" class="social-icon"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#" class="social-icon"><i class="fab fa-instagram"></i></a>
                                        <a href="#" class="social-icon"><i class="fa fa-envelope"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-8 text-center text-sm-left mb-sm-0 mb-3">
                                <div class="row">
                                    <div class="col-12 text-uppercase">
                                        <h4>Quem Somos?</h4>
                                    </div>
                                    <div class="col-12 text-justify">
                                        <p>Somos um anunciente de produtos novos e usados vendidos em Capelinha MG. Não intermediamos as negociações ou realizamos transações financeiras.
                                        </p>
                                        <p>Negócie diretamente com o vendedor forma de pagamento bem como como a entrega diretamente pelo chat.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-sm-3 col-5 ml-lg-auto ml-sm-0 ml-auto mb-sm-0 mb-3">
                                <div class="row">
                                    <div class="col-12 text-uppercase">
                                        <h4>Links Utéis</h4>
                                    </div>
                                    <div class="col-12">
                                        <ul class="footer-nav">
                                            <li>
                                                <a href="#">Ínicio</a>
                                            </li>
                                            <li>
                                                <a href="#">Contate-nos</a>
                                            </li>
                                            <li>
                                                <a href="#">Política e Privacidade</a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1 col-sm-2 col-4 mr-auto mb-sm-0 mb-3">
                                <div class="row">
                                    <div class="col-12 text-uppercase text-underline">
                                        <h4>Ajuda</h4>
                                    </div>
                                    <div class="col-12">
                                        <ul class="footer-nav">
                                            <li>
                                                <a href="#">Como Anunciar?</a>
                                            </li>
                                            <li>
                                                <a href="#">Reportar Vendedor</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </footer>
                <!-- Footer -->
            </div>

        </div>
        <div class="modal" tabindex="-1" role="dialog" id="modal_esqueceu_senha">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Alterar Senha</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" id="formulario_esqueceu_senha" name="formulario_esqueceu_senha">
                            <div class="form-row">
                                <div class="form-group col-md-8 ">
                                    <label for="inputCity">Email :</label>
                                    <div class="iconInput">
                                        <i class="fa fa-envelope"></i>
                                        <input type="email" name="usuarioNewEmail" id="usuarioNewEmail" class="form-control" placeholder="Digite seu email cadastrado">

                                    </div>
                                </div>
                            </div>

                            <div class="form-row ">
                                <div>
                                    <button type="submit" id="btn_solicita_alteracao" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Solicitar</button>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="d-flex flex-row p-2" id="verificar">


                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/js/base.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/js/script.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/js/usuarios.js"></script>
</body>

</html>