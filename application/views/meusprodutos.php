<div class="container ">
    <div class="row d-flex flex-column">
        <div class="col-12 mt-3 mb-2 text-uppercase">
            <h2>Meus produtos</h2>
        </div>
        <div class="mb-4 ml-3">
            <a href="<?php echo base_url(); ?>cadastroproduto"><button type="submit" id="btn_user" class="btn btn-primary"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Novo Produto</button></a>
        </div>

        <div class="col-10 col-xl-8 input_pesquisas mb-3">
            <div class="iconInput">
                <i class="fa fa-search"></i>
                <input type="text" name="pesquisa_usuarios" alt="tabela_usuarios" class="form-control pesquisa" placeholder="Pesquisar Produto...">
            </div>
        </div>
    </div>

    <div class="row">

        <table class="tabela_usuarios col-12" id="tabela_usuarios">

            <tbody>
                <?php
                if (!empty($meusprodutos)) {
                    foreach ($meusprodutos as $prod) {
                        echo '
                           <tr>
                           <td>
                               <div class="col-xl-8 col-10  item-cardapio">
                                   <div class="info-cardapio d-flex flex-row ">
                                       <div class="d-flex flex-row justify-content-start">
                                           <img src="' . base_url() . $prod['produtoImagemPrincipal'] . '" style="width: 100px;heigth:100px;">
                                       </div>
                                       <div class="texto-cardapio d-flex flex-column">
                                           <div style="word-break: break-all;" class="d-flex flex-column justify-content-start">
                                               <h3>' . $prod['produtoNome'] . '</h3>
       
                                               <h6>R$' . $prod['produtoValor'] . '</h6>
                                           </div>
                                           <div class="d-flex flex-row justify-content-start ">
                                               <a href="' . base_url() .'editar?produto='.$prod['produtoId'].'&chave='.$prod['produtoHash'].'&codigo='.$prod['usuarioId'].'" class="mr-1 ml-1"><button class="btn btn-primary"><i class="fa fa-eye"></i></button></a>
                                               <a href="#" class="btn_exclui_produto" produto=' . $prod['produtoId'] . ' chave=' . $prod['produtoHash'] . ' codigo=' . $prod['usuarioId'] . '><button class="btn btn-danger" ><i class="fa fa-trash-alt"></i></button></a>
                                           </div>
                                       </div>
       
                                   </div>
                               </div>
                           </td>
                       </tr>
                           ';
                    }
                }

                ?>


            </tbody>


        </table>







    </div>


</div>

<div class="modal" tabindex="-1" role="dialog" id="modal_avaliacao">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Questionário</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="formulario_questionario" name="formulario_questionario">
                    <div class="form-row">
                        <div class="col-md-8">
                            <label> Este produto foi vendido atráves de anúncio nesse site?</label>
                            <div class="iconInput">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="vendaSite"  value="0">
                                    <label class="form-check-label" for="inlineRadio1">Não</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="vendaSite" value="1" checked>
                                    <label class="form-check-label" for="inlineRadio2">Sim</label>
                                </div>

                            </div>
                        </div>
                    </div><br>

                    
                    <div class="form-row ">
                        <div>
                            <button type="submit" id="btn_salva_questionario" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar</button>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="d-flex flex-row p-2" id="verificar">


                    </div>

                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>