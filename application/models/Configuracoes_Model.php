<?php
class Configuracoes_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	//
	//Retorna as configuracoes de Email
	//
	public function configEmail()
    {
       
        $this->db->from("configuracoesemail");
        $result = $this->db->get();
        if($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return NULL;
        }

    }
	
}
