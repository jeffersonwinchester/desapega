<?php
class Usuarios_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	//
	//Cadastra um usuário
	//
	public function cadastraUsuario($data)
	{
		$this->db->insert("usuarios", $data);
	}


	//
	//Atualiza um usuário
	//
	public function atualizaUsuario($idUsuario, $data)
	{
		$this->db->where("usuarioId", $idUsuario);
		$this->db->update("usuarios", $data);
	}

	//
	//Atualiza um usuário pelo email
	//
	public function atualizaUsuarioEmail($usuarioEmail, $data)
	{
		$this->db->where("usuarioEmail", $usuarioEmail);
		$this->db->update("usuarios", $data);
	}
	//
	//Retorna os dados dos usuários
	//
	public function dadosUsuario($usuarioEmail, $usuarioId)
	{
		if (!empty($usuarioEmail)) {
			$this->db->where("usuarioEmail", $usuarioEmail);
		}
		if (!empty($usuarioId)) {
			$this->db->where("usuarioId", $usuarioId);
		}
		$this->db->from("usuarios");
		$result = $this->db->get();
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return NULL;
		}
	}

	//
	//Cadastra um hash para alterar senha
	//
	public function cadastraHash($data)
	{
		$this->db->insert("alteraSenha", $data);
	}

	//
	//Retorna os dados da tabela alteracao senha
	//
	public function dadosHash($alteraSenhaUsuarioEmail, $alteraSenhaHash)
	{
		if (!empty($alteraSenhaUsuarioEmail)) {
			$this->db->where("alteraSenhaUsuarioEmail", $alteraSenhaUsuarioEmail);
		}
		if (!empty($alteraSenhaHash)) {
			$this->db->where("alteraSenhaHash", $alteraSenhaHash);
		}
		$this->db->from("alteraSenha");
		$result = $this->db->get();
		if ($result->num_rows() > 0) {
			return $result->result_array();
		} else {
			return NULL;
		}
	}

	//
	//Apaga um hash
	//
	public function apagarHash($usuarioEmail)
	{
		$this->db->where("alteraSenhaUsuarioEmail", $usuarioEmail);
		$this->db->delete("alterasenha");
	}


	//
	//Retorna a quantidade de vendas de um usuário
	//
	public function qtdeVendas($usuarioId)
	{

		$this->db->where("usuarioId", $usuarioId);
		$this->db->where("vendaSite", 1);
		$this->db->from("vendas");
		$result = $this->db->get();
		return $result->num_rows();
	}
}
