<?php
class Funcoes_Genericas extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function constroiMenu()
	{
		$this->load->model('Categorias_Model');
		$categorias = $this->Categorias_Model->listaCategorias();
		foreach ($categorias as $categoria) {
			$row[] = '
			<li class="nav-item dropdown" >
            <a  class="nav-link dropdown-toggle" href="#" id="' . $categoria['categoriaNome'] . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . $categoria['categoriaNome'] . '</a>
        	<div class="dropdown-menu" aria-labelledby="' . $categoria['categoriaNome'] . '" >
			';
			$subcategorias = $this->Categorias_Model->listaSubCategorias($categoria['categoriaId']);
			foreach ($subcategorias as  $subcategoria) {
				$titulo_novo = strtolower(preg_replace(
					"/[ -]+/",
					"-",
					strtr(
						utf8_decode(trim($subcategoria['subCategoriaNome'])),
						utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),
						"aaaaeeiooouuncAAAAEEIOOOUUNC-"
					)
				));
				$row[] = '
				<a  class="dropdown-item" href="' . base_url() . 'categoria/' . $titulo_novo . '">' . $subcategoria['subCategoriaNome']  . '</a>
				';
			}
			$row[] = '
				</div>
				</li>
			';
		}
		return $row;
	}

	public function Verificar_Duplicidade($campo,$valor,$id,$tabela)
	{
		if(!empty($id))
		{
			$this->db->where($campo." <>", $id);
		}
		$this->db->where($campo, $valor);
		$this->db->from($tabela);
		
		return $this->db->get()->num_rows() > 0;
	}
}
