<?php  
class Envia_Email extends CI_Model
{
	public function enviar_email($assunto,$destinatario,$mensagem)
	{
		/*$this->load->model("Empresas_Model");
		$dados = $this->Empresas_Model->retornaDadosEmpresa();
        $configuracao = $this->Empresas_Model->retornaConfiguracoes();*/
        $this->load->model("Configuracoes_Model");
        $this->load->library("my_phpmailer");
        $data =  array(
         /*"dados_empresa" => $dados,*/
         "assunto" => $assunto,
         "mensagem" => $mensagem

     );
    $mail = new PHPMailer();
    $mail->IsSMTP(); //Definimos que usaremos o protocolo SMTP para envio.
    $mail->SMTPAuth = true; //Habilitamos a autenticação do SMTP. (true ou false)
     if (ENVIRONMENT=='development') {
        $configuracao = $this->Configuracoes_Model->configEmail()[0];
        $mail->SMTPSecure = "ssl";
        $mail->Host = $configuracao['configuracaoEmailHost']; //Podemos usar o servidor do gMail para enviar.
        $mail->Port = $configuracao['configuracaoEmailPorta']; //Estabelecemos a porta utilizada pelo servidor do gMail.
        $mail->Username = $configuracao['configuracaoEmailEmail']; //Usuário do gMail
        $mail->Password = $configuracao['configuracaoEmailSenha']; //Senha do gMail
        $mail->SetFrom($configuracao['configuracaoEmailEmail'], "Desapega");
        
    }
    else
    {
        $configuracao = $this->Configuracoes_Model->configEmail()[1];
        $mail->Host = $configuracao['configuracaoEmailHost']; //Podemos usar o servidor do gMail para enviar.
        $mail->Port = $configuracao['configuracaoEmailPorta']; //Estabelecemos a porta utilizada pelo servidor do gMail.
        $mail->Username = $configuracao['configuracaoEmailEmail']; //Usuário do gMail
        $mail->Password = $configuracao['configuracaoEmailSenha']; //Senha do gMail
        $mail->SetFrom($configuracao['configuracaoEmailEmail'], "Desapega");
    }
    
    $mail->IsHTML(true); 
    $mail->CharSet = 'UTF-8'; 
    //$mail->AddReplyTo($dados->empresa_email, $dados->empresa_nome); //Para que a resposta será enviada.
    $mail->Subject = $assunto; //Assunto do e-mail.
    $body = $this->load->view('email/email.php',$data,TRUE);
    $mail->Body = $body;
    //$mail->AltBody = "Corpo em texto puro.";
    $destino = $destinatario;
    $mail->AddAddress($destino);

    /*Também é possível adicionar anexos.
    $mail->AddAttachment("images/phpmailer.gif");
    $mail->AddAttachment("images/phpmailer_mini.gif");*/

    if(!$mail->Send()) {
        return false;
    	
    } else {
    	return true;
    }
}
}