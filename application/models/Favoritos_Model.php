<?php
class Favoritos_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //
    //Retorna os favoritos do usuário
    //
    public function retornaFavoritos($usuarioId, $contagem)
    {

        $this->db->from("vfavoritos");
        $this->db->where("usuarioId", $usuarioId);
        $result = $this->db->get();
        if (!empty($contagem)) {
            return $result->num_rows();
        } else {
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return NULL;
            }
        }
    }


       //
	//Verifica se determinado produto existe
	//
	public function VerificaFavorito($produtoId,$produtoHash,$usuarioId,$favoritoId)
    {
       if(!empty($favoritoId))
       {
        $this->db->where("favoritoId",$favoritoId);
       }
        $this->db->where("usuarioId",$usuarioId);
        $this->db->where("produtoId",$produtoId);
        $this->db->where("produtoHash",$produtoHash);
        $this->db->from("vfavoritos");
        $result = $this->db->get();
        return $result->num_rows();

    }


    //Adiciona um novo produto a lista de favoritos

    public function adicionaFavorito($data)
    {
        $this->db->insert("favoritos",$data);
    }

    //Remove um produto da lista de favoritos

    public function removeFavorito($favoritoId)
    {
        $this->db->where("favoritoId", $favoritoId);
		$this->db->delete("favoritos");
    }
}
