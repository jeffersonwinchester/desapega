<?php  
class Categorias_Model extends CI_Model
{
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	//
	//Retorna as categorias cadastradas
	//
	public function listaCategorias(/*$pacienteId*/)
	{
		/*if (!empty($pacienteId)) {
			$this->db->where("paciente_id", $pacienteId);
		}*/
		$this->db->from("categorias");
		$this->db->order_by("categoriaNome", "asc");
		$result = $this->db->get();

		if($result->num_rows() > 0)
		{
			return $result->result_array();
		}
		else
		{
			return NULL;
		}

	}

	//
	//Retorna as subcategorias cadastradas
	//
	public function listaSubCategorias($categoriaId)
        {
            if (!empty($categoriaId)) {
                $this->db->where("categoriaId", $categoriaId);
            }
            $this->db->from("subcategorias");
            $this->db->order_by("subCategoriaNome", "asc");
            $result = $this->db->get();
    
            if($result->num_rows() > 0)
            {
                return $result->result_array();
            }
            else
            {
                return NULL;
            }
    
		}
		
		//
	//Retorna as subcategorias cadastradas
	//
	public function listaSubCategoriasId($subCategoriaId)
	{
		$this->db->where("subCategoriaId", $subCategoriaId);
		$this->db->from("subcategorias");
		$result = $this->db->get();

		if($result->num_rows() > 0)
		{
			return $result->result_array();
		}
		else
		{
			return NULL;
		}

	}

	



	
	

}