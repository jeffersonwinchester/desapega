<?php  
class Produtos_Model extends CI_Model
{
	public function __construct(){
		parent::__construct();
		$this->load->database();
    }
    
    //
	//Retorna os produtos cadastrados da view
	//
	public function listaProdutos($subCategoriaNome,$produtoId,$produtoNome)
    {
        if (!empty($produtoId)) {
            $this->db->where("produtoId", $produtoId);
        }
        if (!empty($produtoNome)) {
            $this->db->where("produtoNome", $produtoNome);
        }
        if (!empty($subCategoriaNome)) {
            $this->db->where("subCategoriaNome", $subCategoriaNome);
        }
        $this->db->from("vprodutos");
        //$this->db->order_by("subCategoriaNome", "asc");
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return NULL;
        }

    }

    //
	//Retorna os produtos cadastrados da tabela produtos
	//
	public function listaProdutosTabela($produtoSelecionado,$produtoNovo)
    {
        if (!empty($produtoSelecionado)) {
            $this->db->where("produtoSelecionado", $produtoSelecionado);
        }
        if (!empty($produtoNovo)) {
            $this->db->order_by("produtoData", "desc");
            $this->db->limit(12);
        }
        $this->db->from("produtos");
        //$this->db->order_by("subCategoriaNome", "asc");
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return NULL;
        }

    }


     //
	//Retorna os produtos cadastrados da view
	//
	public function listaProdutosSimilares($subCategoriaId,$produtoId)
    {
        
        $this->db->where("produtoId<>", $produtoId);
        $this->db->where("subCategoriaId", $subCategoriaId);
        $this->db->from("vprodutos");
        //$this->db->order_by("subCategoriaNome", "asc");
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return NULL;
        }

    }

     //
	//Retorna os produtos cadastrados da view
	//
	public function listaProdutosUsuario($usuarioId,$usuarioHash)
    {
        
        $this->db->where("usuarioId", $usuarioId);
        $this->db->where("usuarioHash", $usuarioHash);
        $this->db->from("vprodutos");
        $this->db->order_by("produtoData", "desc");
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return NULL;
        }

    }

      //
	//Retorna os produtos cadastrados atraves de uma busca
	//
	public function listaProdutosNome($produtoNome)
    {
        
        $this->db->like('produtoNome', $produtoNome);
        $this->db->from("produtos");
        //$this->db->order_by("subCategoriaNome", "asc");
        $result = $this->db->get();

        if($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return NULL;
        }

    }


       //
	//Verifica se determinado produto existe
	//
	public function VerificaProduto($produtoId,$produtoHash,$vendedorId)
    {
        if(!empty($vendedorId))
        {
            $this->db->where("usuarioId",$vendedorId);
        }
        $this->db->where("produtoId",$produtoId);
        $this->db->where("produtoHash",$produtoHash);
        $this->db->from("produtos");
        $result = $this->db->get();
        if($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return NULL;
        }

    }


    //
	//Cadastra um produto
	//
    public function cadastraProduto($data)
    {
        $this->db->insert("produtos",$data);
        $last = $this->db->insert_id();
        return $last;
    }
    
    //
	//Atualiza um produto
	//
    public function atualizaProduto($produtoId,$data)
    {
		$this->db->where("produtoId",$produtoId);
        $this->db->update("produtos",$data);
	}

    public function removeProduto($produtoId,$produtoHash)
    {
        $this->db->where("produtoId", $produtoId);
        $this->db->where("produtoHash", $produtoHash);
		$this->db->delete("produtos");
    }


     //
	//Cadastra onde o produto foi vendido
	//
    public function cadastraVenda($data)
    {
        $this->db->insert("vendas",$data);
       
    }



}