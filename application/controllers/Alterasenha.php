<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alterasenha extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->library("session");
		
	}

	public function index()
	{
		if (empty($_GET['hash']) || empty($_GET['email'])) {
            $flag = 0;
        }
        else
        {
            $flag = 1;
            $this->load->vars(['hash' => $_GET['hash']]);
        }
        $this->load->model('Funcoes_Genericas');
        $data =  array(
			"url" => $flag,
            "menu" => $this->Funcoes_Genericas->constroiMenu(),


		);
			$this->load->view('alterasenha',$data);
		
		
    }

    public function ajax_esqueceu_senha()
    {
        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;
        $json["erros"] = array();
        $data = $this->input->post();
        $this->load->model("Usuarios_Model");
        $this->load->library('form_validation');
        $this->form_validation->set_rules(
            'novaSenha',
            'Senha',
            'required',
            array('required' => 'Nova Senha é obrigatória')
        );
        $this->form_validation->set_rules(
            'confirmSenha',
            'Senha Antiga',
            'required',
            array('required' => 'É obrigatório confirmar a nova senha')
        );
        $this->form_validation->set_rules(
            'hash',
            'Confirma Senha',
            'required',
            array('required' => 'Erro ao atualizar, contate o administrador')
        );
        $this->form_validation->set_rules(
            'email',
            'Confirma Senha',
            'required',
            array('required' => 'Erro ao atualizar, contate o administrador')
        );
       
        if ($this->form_validation->run() == FALSE) {
            $erros = array('mensagens' => validation_errors());
            foreach ($erros as $erro) {
                $retirada = array("</p>", "<p>");
                $json["erros"] = str_replace($retirada, '', $erro);
            }
            $json["status"]=0;
			echo json_encode($json);
			exit;
        }
        $dados = $this->Usuarios_Model->dadosHash($data['email'],$data['hash']);
        if(empty($dados)){
            $json["status"]=0;
			$json["erros"] = "Nenhuma solicitação para alteração encontrada!";
			echo json_encode($json);
			exit;
        }

        if($data['confirmSenha']!=$data['novaSenha'])
        {
            $json["status"]=0;
			$json["erros"] = "As senhas são diferentes!";
			echo json_encode($json);
			exit;
        }
            $data['usuarioSenha']= password_hash($data['novaSenha'],PASSWORD_DEFAULT);
            $usuarioEmail = $data['email'];
            unset($data['confirmSenha']);
            unset($data['novaSenha']);
            unset($data['hash']);
            unset($data['email']);
            $this->Usuarios_Model->atualizaUsuarioEmail($usuarioEmail,$data);
            $this->Usuarios_Model->apagarHash($usuarioEmail);
        


        echo json_encode($json);
    }

    
    

	

}