<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->library("session");
		
	}

	public function index()
	{
		if($this->session->userdata("usuario_id")){
			$this->load->model('Funcoes_Genericas');
			$this->load->model('Favoritos_Model');
			$this->load->model('Mensagens_Model');
			$data =  array(
				"styles" => array(
					"chat.css",	
				),/*
				"scripts" => array(
					"produtos.js",
					"swipe.min.js",
					"popper.min.js",
					"chat.js"		
				),*/
				"titulo" => "Mensagens",
				"menu" => $this->Funcoes_Genericas->constroiMenu(),
				"usuario" => $this->session->userdata("usuario_id"),
				"numerofavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"),1),
				"listagemfavoritos"=> $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"),null),

	
			);
			
			$this->template->show('chat', $data);
		}
		else
		{
			$data2 =  array(
					
				"mensagem" => "Area Restrita, faça login para continuar!"
	
			);
			$this->load->view('login',$data2);
		}
	}

	

}
