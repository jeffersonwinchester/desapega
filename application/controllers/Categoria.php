<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->library("session");
		
	}
	function _remap($param) {
		$this->index($param);
	}

	public function index($param)
	{
		$categoriaEspaco = str_replace('-', ' ', $param);
		$this->load->model('Funcoes_Genericas');
		$this->load->model('Produtos_Model');
		$this->load->model('Favoritos_Model');
		$data =  array(
			/*"styles" => array(
				"estilo.css",			
			),
			"scripts" => array(
				"mascaras.js",
				"paciente.js",		
			),
			*/"titulo" => "Categorias",
			"menu" => $this->Funcoes_Genericas->constroiMenu(),
			"produtos" => $this->Produtos_Model->listaProdutos($categoriaEspaco,null,null),
			"categorianome" =>$categoriaEspaco,
			"usuario" => $this->session->userdata("usuario_id"),
			"numerofavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"),1),


		);
		$this->template->show('categoria',$data);
	}


}
