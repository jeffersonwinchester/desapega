<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cadastroproduto extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library("session");
	}

	public function index()
	{
		if ($this->session->userdata("usuario_id")) {
			$this->load->model('Funcoes_Genericas');
			$this->load->model('Favoritos_Model');
			$this->load->model('Categorias_Model');
			$data =  array(
				"scripts" => array(
					"produtos.js",
					"jquery.maskMoney.js"
				),
				"titulo" => "Cadastro de Produtos",
				"menu" => $this->Funcoes_Genericas->constroiMenu(),
				"usuario" => $this->session->userdata("usuario_id"),
				"numerofavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"), 1),
				"listagemfavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"), null),
				"categorias" => $this->Categorias_Model->listaCategorias()

			);

			$this->template->show('cadastroproduto', $data);
		} else {
			$data2 =  array(

				"mensagem" => "Area Restrita, faça login para continuar!"

			);
			$this->load->view('login', $data2);
		}
	}

	public function ajax_lista_sub()
	{
		if (!$this->input->is_ajax_request()) {
			exit("Nenhum acesso de script direto permitido!");
		}
		$json = array();
		$json["status"] = 1;
		$json["lista"] = array();

		$this->load->model("Categorias_Model");
		$data = $this->input->post();
		$categoriaId = $data['categoria'];
		$texto = '';
		$subcategorias = $this->Categorias_Model->listaSubCategorias($categoriaId);
		foreach ($subcategorias as $subcategoria) {
			$texto = $texto . '<option value="' . $subcategoria["subCategoriaId"] . '">' . $subcategoria["subCategoriaNome"] . '</option>';
		}

		$json["lista"] = $texto;
		echo json_encode($json);
	}


	//Função para importar imagem
	public function ajax_importa_imagem()
	{
		$config["upload_path"] = "./tmp_produtos/";
		$config["allowed_types"] = "png|jpg";
		$config["overwrite"] = TRUE;
		$path = $_FILES['image_file']['name'];
		//$newName = $this->session->userdata("usuario_hash") . $this->session->userdata("usuario_id") . "produto." . pathinfo($path, PATHINFO_EXTENSION);
		//$config['file_name'] = $newName;
		$this->load->library("upload", $config);

		$json = array();
		$json["status"] = 1;

		if (!$this->upload->do_upload("image_file")) {
			$json["error"] = $this->upload->display_errors("", "");
			$json["status"] = 0;
		} else {
			if ($this->upload->data()["file_size"] <= 3072) {

				$file_name = $this->upload->data()["file_name"];
				$json["img_path"] = base_url() . "tmp_produtos/" . $file_name;
			} else {
				$json["error"] = "Arquivo nao deve ser maior que 3 mb";
				$json["status"] = 0;
			}
		}
		echo json_encode($json);
	}


	public function ajax_cadastra_produto()
    {
        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;
        $json["erros"] = array();
        $data = $this->input->post();
        $this->load->model("Produtos_Model");
        $this->load->library('form_validation');
        $this->form_validation->set_rules(
            'produtoNome',
            'Nome',
            'required',
            array('required' => 'Nome do Produto é obrigatório')
        );
        $this->form_validation->set_rules(
            'produtoValor',
            'Valor',
            'required',
            array('required' => 'Valor do produto é obrigatório')
        );
        $this->form_validation->set_rules(
            'produto_promocao',
            'promocao',
            'required',
            array('required' => 'É obrigatório informar se o produto está na promoção')
        );
        $this->form_validation->set_rules(
            'produtoDescricao',
            'Descricao',
            'required',
            array('required' => 'Descrição do produto é obrigatório')
        );
        $this->form_validation->set_rules(
            'produtoCategoria',
            'Categoria',
            'required',
            array('required' => 'Categoria do produto é obrigatório')
        );
        $this->form_validation->set_rules(
            'subCategoriaId',
            'subCategoria',
            'required',
            array('required' => 'Sub Categoria do produto é obrigatório')
		);
		$this->form_validation->set_rules(
            'produtoImagemPrincipal',
            'Imagem',
            'required',
            array('required' => 'Imagem Principal do produto é obrigatório')
        );
        if ($this->form_validation->run() == FALSE) {
            $erros = array('mensagens' => validation_errors());
            foreach ($erros as $erro) {
                $retirada = array("</p>", "<p>");
                $json["erros"] = str_replace($retirada, '', $erro);
			}
			$json["status"] = 0;
            echo json_encode($json);
            exit;
		}
		
		if($data["produto_promocao"]==1)
		{
			if ($data["produtoValorPromocao"]>$data["produtoValor"]) {
				$json["erros"] = "O valor do produto em promoção deve ser maior que o valor do produto sem esta";
				$json["status"] = 0;
				echo json_encode($json);
				exit;
			}
		}
		if (!empty($data["produtoValorPromocao"])) {
			$data["produtoValorPromocao"] = str_replace(',', '.', $data["produtoValorPromocao"]);
		}

		$dataImagem  =  array(
			'produtoImagemPrincipal' => $data["produtoImagemPrincipal"],
			'produtoImagem1'=>$data["produtoImagem1"],
			'produtoImagem2'=>$data["produtoImagem2"]

		); 
		$hash = rand(111111111,999999999);
		unset($data["produtoImagemPrincipal"]);
		unset($data["produtoImagem1"]);
		unset($data["produtoImagem2"]);
		unset($data["produto_promocao"]);
		unset($data["produtoCategoria"]);
		$data["produtoValor"] = str_replace(',', '.', $data["produtoValor"]);
		$data["produtoHash"] = password_hash($hash,PASSWORD_DEFAULT);
		$data["produtoData"]= date("Y-m-d H:i:s");
		$data["usuarioId"] = $this->session->userdata("usuario_id");
		$ultimo = $this->Produtos_Model->cadastraProduto($data);
		$produto=$this->Produtos_Model->listaProdutos(null,$ultimo,null)[0];
		

		$caminho=str_replace('\\','/',getcwd());
		mkdir($caminho."/public/img/produtos/".$ultimo,0777, true);
    	if (!empty($dataImagem["produtoImagemPrincipal"])) {

				$file_name = basename($dataImagem["produtoImagemPrincipal"]);
				$extensao = substr($dataImagem["produtoImagemPrincipal"], -4);
				$novonome=$produto["produtoHash"].$ultimo."1".$extensao;
				$old_path = getcwd() . "/tmp_produtos/" . $file_name;
				$new_path = getcwd() . "/public/img/produtos/".$ultimo."/" . $novonome;
				if(rename($old_path, $new_path))
				{
					$dataImagem["produtoImagemPrincipal"] = "public/img/produtos/".$ultimo."/" . $novonome;
				}

				

			} else {
				unset($dataImagem["produtoImagemPrincipal"]);
			}

			if (!empty($dataImagem["produtoImagem1"])) {

				$file_name = basename($dataImagem["produtoImagem1"]);
				$extensao = substr($dataImagem["produtoImagem1"], -4);
				$novonome=$produto["produtoHash"].$ultimo."2".$extensao;
				$old_path = getcwd() . "/tmp_produtos/" . $file_name;
				$new_path = getcwd() . "/public/img/produtos/".$ultimo."/" . $novonome;
				if(rename($old_path, $new_path))
				{
					$dataImagem["produtoImagem1"] = "public/img/produtos/".$ultimo."/" . $novonome;
				}

				

			} else {
				unset($dataImagem["produtoImagem1"]);
			}

			if (!empty($dataImagem["produtoImagem2"])) {

				$file_name = basename($dataImagem["produtoImagem2"]);
				$extensao = substr($dataImagem["produtoImagem2"], -4);
				$novonome=$produto["produtoHash"].$ultimo."3".$extensao;
				$old_path = getcwd() . "/tmp_produtos/" . $file_name;
				$new_path = getcwd() . "/public/img/produtos/".$ultimo."/" . $novonome;
				if(rename($old_path, $new_path))
				{
					$dataImagem["produtoImagem2"] = "public/img/produtos/".$ultimo."/" . $novonome;
				}

				

			} else {
				unset($dataImagem["produtoImagem2"]);
			}

			$this->Produtos_Model->atualizaProduto($ultimo,$dataImagem);

		echo json_encode($json);
	}

	
	
	
	



}
