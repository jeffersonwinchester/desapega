<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usuario extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("session");
    }

    public function index()
    {
        if ($this->session->userdata("usuario_id")) {
            $this->load->model('Funcoes_Genericas');
            $this->load->model('Favoritos_Model');
            $this->load->model('Mensagens_Model');
            $this->load->model('Usuarios_Model');
            $data =  array(
                "styles" => array(
                    "chat.css",
                ),
                "scripts" => array(
                    "usuarios.js"
                ),
                "titulo" => "Meus Dados",
                "menu" => $this->Funcoes_Genericas->constroiMenu(),
                "usuario" => $this->session->userdata("usuario_id"),
                "numerofavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"), 1),
                "listagemfavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"), null),
                "dados" => $this->Usuarios_Model->dadosUsuario(null, $this->session->userdata("usuario_id"))[0]


            );

            $this->template->show('usuario', $data);
        } else {
            $data2 =  array(

                "mensagem" => "Area Restrita, faça login para continuar!"

            );
            $this->load->view('login', $data2);
        }
    }


    public function ajax_atualiza_usuario()
    {
        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;
        $json["erros"] = array();
        $data = $this->input->post();
        $this->load->model("Usuarios_Model");
        $this->load->library('form_validation');
        $this->form_validation->set_rules(
            'usuarioNome',
            'Nome',
            'required',
            array('required' => 'Nome de Usuário é obrigatório')
        );
        $this->form_validation->set_rules(
            'usuarioRua',
            'Rua',
            'required',
            array('required' => 'Nome da Rua é obrigatório')
        );
        $this->form_validation->set_rules(
            'usuarioNumero',
            'Número',
            'required',
            array('required' => 'Número da casa é obrigatório')
        );
        $this->form_validation->set_rules(
            'usuarioBairro',
            'Bairro',
            'required',
            array('required' => 'Bairro é obrigatório')
        );
        $this->form_validation->set_rules(
            'usuarioCidade',
            'Cidade',
            'required',
            array('required' => 'Cidade é obrigatório')
        );
        $this->form_validation->set_rules(
            'usuarioEstado',
            'Estado',
            'required',
            array('required' => 'Estado é obrigatório')
        );
        if ($this->form_validation->run() == FALSE) {
            $erros = array('mensagens' => validation_errors());
            foreach ($erros as $erro) {
                $retirada = array("</p>", "<p>");
                $json["erros"] = str_replace($retirada, '', $erro);
            }
        }


        if (!empty($json['erros'])) {
            $json['status'] = 0;
        } else {
            if (!empty($data["usuarioFoto"])) {

				$file_name = basename($data["usuarioFoto"]);
				$old_path = getcwd() . "/tmp_perfil/" . $file_name;
				$new_path = getcwd() . "/public/img/perfil/" . $file_name;
				rename($old_path, $new_path);

				$data["usuarioFoto"] = "public/img/perfil/" . $file_name;

			} else {
				unset($data["usuarioFoto"]);
			}

            unset($data['usuarioSenha']);
            unset($data['usuarioEmail']);
            $idUsuario = $this->session->userdata("usuario_id");
            $this->Usuarios_Model->atualizaUsuario($idUsuario, $data);
        }


        echo json_encode($json);
    }


    public function ajax_atualiza_senha()
    {
        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;
        $json["erros"] = array();
        $data = $this->input->post();
        $this->load->model("Usuarios_Model");
        $this->load->library('form_validation');
        $this->form_validation->set_rules(
            'usuarioSenha',
            'Senha',
            'required',
            array('required' => 'Nova Senha é obrigatória')
        );
        $this->form_validation->set_rules(
            'usuarioSenhaAntiga',
            'Senha Antiga',
            'required',
            array('required' => 'Senha antiga é obrigatória')
        );
        $this->form_validation->set_rules(
            'usuarioConfirmaSenha',
            'Confirma Senha',
            'required',
            array('required' => 'É obrigatório confirmar a nova senha')
        );

        if ($this->form_validation->run() == FALSE) {
            $erros = array('mensagens' => validation_errors());
            foreach ($erros as $erro) {
                $retirada = array("</p>", "<p>");
                $json["erros"] = str_replace($retirada, '', $erro);
            }
            $json["status"] = 0;
            echo json_encode($json);
            exit;
        }
        $user = $this->Usuarios_Model->dadosUsuario(null, $this->session->userdata("usuario_id"))[0];
        if (!password_verify($data['usuarioSenhaAntiga'], $user['usuarioSenha'])) {
            $json["status"] = 0;
            $json["erros"] = "Senha antiga incorreta!";
            echo json_encode($json);
            exit;
        }

        if ($data['usuarioSenha'] != $data['usuarioConfirmaSenha']) {
            $json["status"] = 0;
            $json["erros"] = "As senhas são diferentes!";
            echo json_encode($json);
            exit;
        }

        
        unset($data['usuarioConfirmaSenha']);
        unset($data['usuarioSenhaAntiga']);
        $data['usuarioSenha'] = password_hash($data['usuarioSenha'], PASSWORD_DEFAULT);
        $idUsuario = $this->session->userdata("usuario_id");
        $this->Usuarios_Model->atualizaUsuario($idUsuario, $data);



        echo json_encode($json);
    }


    //Função para importar imagem
    public function ajax_importa_imagem()
    {
        
        $config["upload_path"] = "./tmp_perfil/";
        $config["allowed_types"] = "png|jpg";
        $config["overwrite"] = TRUE;
        $path = $_FILES['image_file']['name'];
        $newName = $this->session->userdata("usuario_hash") .$this->session->userdata("usuario_id"). "." . pathinfo($path, PATHINFO_EXTENSION);
        $config['file_name'] = $newName;
        $this->load->library("upload", $config);

        $json = array();
        $json["status"] = 1;

        if (!$this->upload->do_upload("image_file")) {
            $json["error"] = $this->upload->display_errors("", "");
            $json["status"] = 0;
        } else {
            if ($this->upload->data()["file_size"] <= 3078) {

                $file_name = $this->upload->data()["file_name"];

                $json["img_path"] = base_url() . "tmp_perfil/" . $file_name;
            } else {
                $json["error"] = "Arquivo nao deve ser maior que 3 mb";
                $json["status"] = 0;
            }
        }
        echo json_encode($json);
    }
}
