<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Favoritos extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library("session");
	}

	public function index()
	{
		if ($this->session->userdata("usuario_id")) {
			$this->load->model('Funcoes_Genericas');
			$this->load->model('Favoritos_Model');
			$data =  array(
				"scripts" => array(
					"produtos.js",
				),
				"titulo" => "Favoritos",
				"menu" => $this->Funcoes_Genericas->constroiMenu(),
				"usuario" => $this->session->userdata("usuario_id"),
				"numerofavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"), 1),
				"listagemfavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"), null)

			);

			$this->template->show('favoritos', $data);
		} else {
			$data2 =  array(

				"mensagem" => "Area Restrita, faça login para continuar!"

			);
			$this->load->view('login', $data2);
		}
	}
	public function ajax_adiciona_favorito()
	{
		if (!$this->input->is_ajax_request()) {
			exit("Nenhum acesso de script direto permitido!");
		}
		$json = array();
		$json["status"] = 1;
		$json["erros"] = array();

		$this->load->model("Favoritos_Model");
		$this->load->model("Produtos_Model");
		$data = $this->input->post();

		if (!$this->session->userdata("usuario_id")) {
			$json["status"] = 0;
			$json["erros"] = "É preciso fazer login para continuar!";
			echo json_encode($json);
			exit;
		}
		$verificaproduto = $this->Produtos_Model->VerificaProduto($data["produto"], $data["chave"], null);
		if (empty($verificaproduto)) {
			$json["status"] = 0;
			$json["erros"] = "Produto não existe!";
			echo json_encode($json);
			exit;
		} else {
			$verificaproduto = $verificaproduto[0];
			if ($verificaproduto['usuarioId'] == $this->session->userdata("usuario_id")) {
				$json["status"] = 0;
				$json["erros"] = "Não é possível adicionar seu próprio produto aos favoritos!";
				echo json_encode($json);
				exit;
			}
		}
		if ($this->Favoritos_Model->VerificaFavorito($data["produto"], $data["chave"], $this->session->userdata("usuario_id"), null) > 0) {
			$json["status"] = 0;
			$json["erros"] = "Produto já adicionado aos favoritos!";
			echo json_encode($json);
			exit;
		}

		$dia = $today = date("Y-m-d");
		$data['produtoId'] = $data["produto"];
		$data['usuarioId'] = $this->session->userdata("usuario_id");
		$data['favoritoDataAdicao'] = $dia;
		unset($data["chave"]);
		unset($data["produto"]);
		$this->Favoritos_Model->adicionaFavorito($data);

		echo json_encode($json);
	}

	public function ajax_remove_favorito()
	{
		if (!$this->input->is_ajax_request()) {
			exit("Nenhum acesso de script direto permitido!");
		}
		$json = array();
		$json["status"] = 1;
		$json["erros"] = array();

		$this->load->model("Favoritos_Model");
		$this->load->model("Produtos_Model");
		$data = $this->input->post();
		$verificaproduto = $this->Produtos_Model->VerificaProduto($data["produto"], $data["chave"], null);
		if (empty($verificaproduto)) {
			$json["status"] = 0;
			$json["erros"] = "Produto não existe!";
			echo json_encode($json);
			exit;
		}
		if ($this->Favoritos_Model->VerificaFavorito($data["produto"], $data["chave"], $this->session->userdata("usuario_id"), $data["favorito"]) == 0) {
			$json["status"] = 0;
			$json["erros"] = "Produto não existe nos seus favoritos!";
			echo json_encode($json);
			exit;
		}

		$favoritoId = $data["favorito"];
		$data['usuarioId'] = $this->session->userdata("usuario_id");
		$this->Favoritos_Model->removeFavorito($favoritoId);


		echo json_encode($json);
	}
}
