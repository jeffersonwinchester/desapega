<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library("session");
	}

	public function index()
	{
		if ($this->session->userdata("usuario_id")) {

			echo "
			<body onload='window.history.back();'>
			";
		} else {
			$this->load->model('Funcoes_Genericas');
			$data =  array(
				"menu" => $this->Funcoes_Genericas->constroiMenu(),

			);
			$this->load->view('login', $data);
		}
	}


	//Função para efetuar login no sistema
	public function ajax_efetua_login()
	{
		if (!$this->input->is_ajax_request()) {
			exit("Acesso direto ao script não permitido !");
		}

		$json = array();
		$json["status"] = 1;
		$json["url"] = 1;
		$json["erros"] = array();
		$data = $this->input->post();
		$this->load->library('form_validation');
		$this->load->model('Usuarios_Model');
		$this->form_validation->set_rules(
			'usuariosenha',
			'Senha',
			'required',
			array('required' => 'Senha é obrigatório')
		);
		$this->form_validation->set_rules('usuarioEmail', 'Email', 'required|valid_email', array('required' => 'Email é obrigatório', 'valid_email' => 'Endereço de email inválido!'));
		if ($this->form_validation->run() == FALSE) {
			$erros = array('mensagens' => validation_errors());
			foreach ($erros as $erro) {
				$retirada = array("</p>", "<p>");
				$json["status"] = 0;
				$json["erros"] = str_replace($retirada, '', $erro);
			}
		}
		$result = $this->Usuarios_Model->dadosUsuario($data['usuarioEmail'], null);


		if ($result) {
			$dados = $result[0];
			$usuarioId = $dados['usuarioId'];
			$usuarioSenha = $dados['usuarioSenha'];
			$usuarioNome = $dados['usuarioNome'];
			$usuarioHash = $dados['usuarioHash'];
			//password verify
			if (password_verify($data['usuarioSenha'], $usuarioSenha)) {

				$this->session->set_userdata("usuario_id", $usuarioId);
				$this->session->set_userdata("usuario_nome", $usuarioNome);
				$this->session->set_userdata("usuario_hash", $usuarioHash);
				$json["url"] = base_url() . "inicio";
				$json["status"] = 1;
			} else {
				$json["status"] = 0;
				$json["erros"] = 'Senha Incorreta!';
			}
		} else {
			$json["status"] = 0;
			$json["erros"] = 'Email não encontrado!';
		}


		echo json_encode($json);
	}
	//Função para sair do sistema
	public function logoff()
	{
		$this->session->sess_destroy();
		header("Location: " . base_url() . "login");
	}

	//Função para criar url e mandar para atualizar senha
	public function ajax_esqueceu_senha()
	{
		if (!$this->input->is_ajax_request()) {
			exit("Nenhum acesso de script direto permitido!");
		}

		$json = array();
		$json["status"] = 1;
		$json["erros"] = array();
		$data = $this->input->post();
		$this->load->model("Usuarios_Model");
		$this->load->library('form_validation');
		$this->form_validation->set_rules('usuarioNewEmail', 'Email', 'required|valid_email', array('required' => 'Email é obrigatório', 'valid_email' => 'Endereço de email inválido!'));
		if ($this->form_validation->run() == FALSE) {
			$erros = array('mensagens' => validation_errors());
			foreach ($erros as $erro) {
				$retirada = array("</p>", "<p>");
				$json["erros"] = str_replace($retirada, '', $erro);
			}
			$json["status"] = 0;
			echo json_encode($json);
			exit;
		}


		$user = $this->Usuarios_Model->dadosUsuario($data['usuarioNewEmail'], null);
		$verificahash = $this->Usuarios_Model->dadosHash($data['usuarioNewEmail'], null);
		if (empty($user)) {
			$json["status"] = 0;
			$json["erros"] = "Email não encontrado!";
			echo json_encode($json);
			exit;
		}
		if (!empty($verificahash)) {
			$json["status"] = 0;
			$json["erros"] = "Uma link para alteração de senha já foi enviado para o email informado!";
			echo json_encode($json);
			exit;
		}

		$hash = rand(111111111, 999999999);
		$hash_cript = password_hash($hash, PASSWORD_DEFAULT);
		$url = base_url() . 'alterasenha?hash=' . $hash_cript . '&email=' . $data['usuarioNewEmail'] . '';
		$mensagem = 'Prezado usuário, segue abaixo um link para alteração de senha<br>
						<a href=' . $url . '>Clique aqui</a>';
		$this->load->model("Envia_Email");
		if ($this->Envia_Email->enviar_email('Alteração de senha', $data['usuarioNewEmail'], $mensagem)) {
			$data['alteraSenhaHash'] = $hash_cript;
			$data['alteraSenhaUsuarioEmail'] = $data['usuarioNewEmail'];
			unset($data['usuarioNewEmail']);
			$this->Usuarios_Model->cadastraHash($data);
		} else {
			$json['status'] = 0;
			$json["erros"] = 'Erro ao enviar link, tente novamente mais tarde!';
		}


		echo json_encode($json);
	}
}
