<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->library("session");
		
	}
	function _remap($param) {
		$this->index($param);
	}

	public function index($param)
	{
        if(empty($_GET['produto']))
        {
            $produtos = null;
            $titulobusca = null;
        }
        else
        {
            $this->load->model('Produtos_Model');
            $produtos = $this->Produtos_Model->listaProdutosNome($_GET['produto']);
            $titulobusca = 'Buscando por '.$_GET['produto'];
        }
		$this->load->model('Funcoes_Genericas');
		$this->load->model('Favoritos_Model');
		
		$data =  array(
			/*"styles" => array(
				"estilo.css",			
			),
			"scripts" => array(
				"mascaras.js",
				"paciente.js",		
			),*/
			"titulo" => "Busca Produtos",
            "menu" => $this->Funcoes_Genericas->constroiMenu(),
            "buscaprodutos" => $produtos,
			"busca" => $titulobusca,
			"usuario" => $this->session->userdata("usuario_id"),
			"numerofavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"),1),



		);
		$this->template->show('produtos',$data);
	}


}
