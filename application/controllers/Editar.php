<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Editar extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library("session");
	}

	public function index()
	{
		if ($this->session->userdata("usuario_id")) {
            $this->load->model('Categorias_Model');
            $this->load->model('Produtos_Model');
            if (empty($_GET['produto'])||empty($_GET['chave'])||empty($_GET['codigo'])) {
                $produto = null;
            }
            else
            {
                $produto = $this->Produtos_Model->VerificaProduto($_GET['produto'],$_GET['chave'],$_GET['codigo']);
                
            }

            if (empty($produto)) {
                $cat = null;
                $cat2 = null;
            }
            else
            {
                $produto = $produto[0];
                $cat = $this->Categorias_Model->listaSubCategoriasId($produto['subCategoriaId'])[0];
                $cat2 = $this->Categorias_Model->listaSubCategoriasId($produto['subCategoriaId']);
                
            }
			$this->load->model('Funcoes_Genericas');
			$this->load->model('Favoritos_Model');
			
			$data =  array(
				"scripts" => array(
					"jquery.maskMoney.js",
					"produtos.js"
				),
				"titulo" => "Meus Produtos",
				"menu" => $this->Funcoes_Genericas->constroiMenu(),
				"usuario" => $this->session->userdata("usuario_id"),
				"numerofavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"), 1),
				"listagemfavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"), null),
                "meusprodutos" => $this->Produtos_Model->listaProdutosUsuario($this->session->userdata("usuario_id"), $this->session->userdata("usuario_hash")),
                "categorias" => $this->Categorias_Model->listaCategorias(),
                "listaproduto" => $produto,
                "subcategoria"=> $cat,
                "subcategorias"=> $cat2

			);

			$this->template->show('editar', $data);
		} else {
			$data2 =  array(

				"mensagem" => "Area Restrita, faça login para continuar!"

			);
			$this->load->view('login', $data2);
		}
    }
    
    public function ajax_edita_produto()
    {
        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;
        $json["erros"] = array();
        $data = $this->input->post();
        $this->load->model("Produtos_Model");
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules(
            'produtoNome',
            'Nome',
            'required',
            array('required' => 'Nome do Produto é obrigatório')
        );
        $this->form_validation->set_rules(
            'produtoValor',
            'Valor',
            'required',
            array('required' => 'Valor do produto é obrigatório')
        );
        $this->form_validation->set_rules(
            'produto_promocao',
            'promocao',
            'required',
            array('required' => 'É obrigatório informar se o produto está na promoção')
        );
        $this->form_validation->set_rules(
            'produtoDescricao',
            'Descricao',
            'required',
            array('required' => 'Descrição do produto é obrigatório')
        );
        $this->form_validation->set_rules(
            'produtoCategoria',
            'Categoria',
            'required',
            array('required' => 'Categoria do produto é obrigatório')
        );
        $this->form_validation->set_rules(
            'subCategoriaId',
            'subCategoria',
            'required',
            array('required' => 'Sub Categoria do produto é obrigatório')
		);
        if ($this->form_validation->run() == FALSE) {
            $erros = array('mensagens' => validation_errors());
            foreach ($erros as $erro) {
                $retirada = array("</p>", "<p>");
                $json["erros"] = str_replace($retirada, '', $erro);
			}
			$json["status"] = 0;
            echo json_encode($json);
            exit;
        }
        $verificaproduto = $this->Produtos_Model->VerificaProduto($data["produto"],$data["chave"],$data["codigo"]);
        if (empty($verificaproduto)) {
            $json["erros"] = "Produto não existe";
				$json["status"] = 0;
				echo json_encode($json);
				exit;
        }

        if ($data["codigo"]!=$this->session->userdata("usuario_id")) {
            $json["erros"] = "Acesso não autorizado";
            $json["status"] = 0;
            echo json_encode($json);
            exit;
        }
		
		if($data["produto_promocao"]==1)
		{
			if ($data["produtoValorPromocao"]>$data["produtoValor"]) {
				$json["erros"] = "O valor do produto em promoção deve ser maior que o valor do produto sem esta";
				$json["status"] = 0;
				echo json_encode($json);
				exit;
			}
		}
		if (!empty($data["produtoValorPromocao"])) {
			$data["produtoValorPromocao"] = str_replace(',', '.', $data["produtoValorPromocao"]);
        }
        else
        {
            $data["produtoValorPromocao"] = null;
        }
        $dataImagem  =  array(
			'produtoImagemPrincipal' => $data["produtoImagemPrincipal"],
			'produtoImagem1'=>$data["produtoImagem1"],
			'produtoImagem2'=>$data["produtoImagem2"]

		); 
        $id = $data['produto'];
        $chave = $data['chave'];
        unset($data['produto']);
        unset($data['chave']);
        unset($data['codigo']);
        unset($data["produtoImagemPrincipal"]);
		unset($data["produtoImagem1"]);
		unset($data["produtoImagem2"]);
		unset($data["produto_promocao"]);
		unset($data["produtoCategoria"]);
        $data["produtoValor"] = str_replace(',', '.', $data["produtoValor"]);
        $this->Produtos_Model->atualizaProduto($id,$data);
        if (!empty($dataImagem["produtoImagemPrincipal"])) {

            $file_name = basename($dataImagem["produtoImagemPrincipal"]);
            $extensao = substr($dataImagem["produtoImagemPrincipal"], -4);
            $novonome=$chave.$id."1".$extensao;
            $old_path = getcwd() . "/tmp_produtos/" . $file_name;
            $new_path = getcwd() . "/public/img/produtos/".$id."/" . $novonome;
            rename($old_path, $new_path);

            $dataImagem["produtoImagemPrincipal"] = "public/img/produtos/".$id."/" . $novonome;

        } else {
            unset($dataImagem["produtoImagemPrincipal"]);
        }

        if (!empty($dataImagem["produtoImagem1"])) {

            $file_name = basename($dataImagem["produtoImagem1"]);
            $extensao = substr($dataImagem["produtoImagem1"], -4);
            $novonome=$chave.$id."2".$extensao;
            $old_path = getcwd() . "/tmp_produtos/" . $file_name;
            $new_path = getcwd() . "/public/img/produtos/".$id."/" . $novonome;
            rename($old_path, $new_path);

            $dataImagem["produtoImagem1"] = "public/img/produtos/".$id."/" . $novonome;

        } else {
            unset($dataImagem["produtoImagem1"]);
        }

        if (!empty($dataImagem["produtoImagem2"])) {

            $file_name = basename($dataImagem["produtoImagem2"]);
            $extensao = substr($dataImagem["produtoImagem2"], -4);
            $novonome=$chave.$id."3".$extensao;
            $old_path = getcwd() . "/tmp_produtos/" . $file_name;
            $new_path = getcwd() . "/public/img/produtos/".$id."/" . $novonome;
            rename($old_path, $new_path);

            $dataImagem["produtoImagem2"] = "public/img/produtos/".$id."/" . $novonome;

        } else {
            unset($dataImagem["produtoImagem2"]);
        }
        if (!empty($dataImagem)) {
            $this->Produtos_Model->atualizaProduto($id,$dataImagem);
        }
        /*

		
		

		$caminho=str_replace('\\','/',getcwd());
		mkdir($caminho."/public/img/produtos/".$ultimo,0777, true);
    	if (!empty($dataImagem["produtoImagemPrincipal"])) {

				$file_name = basename($dataImagem["produtoImagemPrincipal"]);
				$extensao = substr($dataImagem["produtoImagemPrincipal"], -4);
				$novonome=$produto["produtoHash"].$ultimo."1".$extensao;
				$old_path = getcwd() . "/tmp_produtos/" . $file_name;
				$new_path = getcwd() . "/public/img/produtos/".$ultimo."/" . $novonome;
				rename($old_path, $new_path);

				$dataImagem["produtoImagemPrincipal"] = "public/img/produtos/".$ultimo."/" . $novonome;

			} else {
				unset($dataImagem["produtoImagemPrincipal"]);
			}

			if (!empty($dataImagem["produtoImagem1"])) {

				$file_name = basename($dataImagem["produtoImagem1"]);
				$extensao = substr($dataImagem["produtoImagem1"], -4);
				$novonome=$produto["produtoHash"].$ultimo."2".$extensao;
				$old_path = getcwd() . "/tmp_produtos/" . $file_name;
				$new_path = getcwd() . "/public/img/produtos/".$ultimo."/" . $novonome;
				rename($old_path, $new_path);

				$dataImagem["produtoImagem1"] = "public/img/produtos/".$ultimo."/" . $novonome;

			} else {
				unset($dataImagem["produtoImagem1"]);
			}

			if (!empty($dataImagem["produtoImagem2"])) {

				$file_name = basename($dataImagem["produtoImagem2"]);
				$extensao = substr($dataImagem["produtoImagem2"], -4);
				$novonome=$produto["produtoHash"].$ultimo."3".$extensao;
				$old_path = getcwd() . "/tmp_produtos/" . $file_name;
				$new_path = getcwd() . "/public/img/produtos/".$ultimo."/" . $novonome;
				rename($old_path, $new_path);

				$dataImagem["produtoImagem2"] = "public/img/produtos/".$ultimo."/" . $novonome;

			} else {
				unset($dataImagem["produtoImagem2"]);
			}

            $this->Produtos_Model->atualizaProduto($ultimo,$dataImagem);*/

		echo json_encode($json);
    }
    
    


	

}
