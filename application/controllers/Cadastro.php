<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cadastro extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library("session");
	}

	public function index()
	{
		$this->load->model('Funcoes_Genericas');
		$data =  array(
			"menu" => $this->Funcoes_Genericas->constroiMenu(),

		);
		$this->load->view('cadastro', $data);
	}

	public function ajax_cadastra_usuario()
	{
		if (!$this->input->is_ajax_request()) {
			exit("Nenhum acesso de script direto permitido!");
		}

		$json = array();
		$json["status"] = 1;
		$json["erros"] = array();
		$data = $this->input->post();
		$this->load->model("Funcoes_Genericas");
		$this->load->model("Usuarios_Model");
		$this->load->library('form_validation');
		$this->form_validation->set_rules(
			'usuarioNome',
			'Nome',
			'required',
			array('required' => 'Nome de Usuário é obrigatório')
		);
		$this->form_validation->set_rules(
			'usuarioRua',
			'Rua',
			'required',
			array('required' => 'Nome da Rua é obrigatório')
		);
		$this->form_validation->set_rules(
			'usuarioNumero',
			'Número',
			'required',
			array('required' => 'Número da casa é obrigatório')
		);
		$this->form_validation->set_rules(
			'usuarioBairro',
			'Bairro',
			'required',
			array('required' => 'Bairro é obrigatório')
		);
		$this->form_validation->set_rules(
			'usuarioCidade',
			'Cidade',
			'required',
			array('required' => 'Cidade é obrigatório')
		);
		$this->form_validation->set_rules(
			'usuarioEstado',
			'Estado',
			'required',
			array('required' => 'Estado é obrigatório')
		);
		$this->form_validation->set_rules('usuarioEmail', 'Email', 'required|valid_email', array('required' => 'Email é obrigatório', 'valid_email' => 'Endereço de email inválido!'));
		if ($this->form_validation->run() == FALSE) {
			$erros = array('mensagens' => validation_errors());
			foreach ($erros as $erro) {
				$retirada = array("</p>", "<p>");
				$json["erros"] = str_replace($retirada, '', $erro);
			}
		}
		if (empty($data['termos'])) {
			$json["erros"] = 'É preciso aceitar os termos e condições!';
		}
		if ($this->Funcoes_Genericas->Verificar_Duplicidade("usuarioEmail", $data["usuarioEmail"], null, "usuarios")) {
			$json["erros"] = 'Email já cadastrado no sistema!';
		}

		if (!empty($json['erros'])) {
			$json['status'] = 0;
		} else {
			$senha = rand(111111111, 999999999);
			$hash = rand(111111111, 999999999);
			$mensagem = "Prezado usuário, segue abaixo sua senha para acesso ao nosso sistema<br>
						<b>Senha: </b>$senha<br>Altere sua senha após fazer login para sua segurança.";
			$this->load->model("Envia_Email");
			if ($this->Envia_Email->enviar_email('Cadastro no Desapega', $data['usuarioEmail'], $mensagem)) {
				$senha_criptografada = password_hash($senha, PASSWORD_DEFAULT);
				$data['usuarioSenha'] = $senha_criptografada;
				$data['usuarioFoto'] = "public/img/perfil/sem_foto.png";
				$data['usuarioHash'] = password_hash($hash, PASSWORD_DEFAULT);
				unset($data['termos']);
				$this->Usuarios_Model->cadastraUsuario($data);
			} else {
				$json['status'] = 0;
				$json["erros"] = 'Erro ao realizar cadastro, tente novamente mais tarde!';
			}
		}


		echo json_encode($json);
	}
}
