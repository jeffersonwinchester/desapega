<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->library("session");
		
	}

	public function index()
	{
		$this->load->model('Funcoes_Genericas');
		$this->load->model('Produtos_Model');
		$this->load->model('Favoritos_Model');
		$data =  array(
			/*"styles" => array(
				"estilo.css",			
			),
			"scripts" => array(
				"mascaras.js",
				"paciente.js",		
			),*/
			"titulo" => "Inicio",
			"menu" => $this->Funcoes_Genericas->constroiMenu(),
			"produtosselecionados" => $this->Produtos_Model->listaProdutosTabela(1,null),
			"produtosnovos" => $this->Produtos_Model->listaProdutosTabela(null,1),
			"usuario" => $this->session->userdata("usuario_id"),
			"numerofavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"),1),


		);
		
		$this->template->show('inicio',$data);
	}

	


}
