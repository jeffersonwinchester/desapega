<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produto extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library("session");
	}

	public function index()
	{
		$this->load->model('Favoritos_Model');
		$this->load->model('Funcoes_Genericas');
		$data =  array(
			"scripts" => array(
				"produtos.js",		
			),
			"titulo" => "Produtos",
			"menu" => $this->Funcoes_Genericas->constroiMenu(),
			"produtoinformacao" => $this->dadosProduto(),
			"usuario" => $this->session->userdata("usuario_id"),
			"numerofavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"),1),


		);
		$this->template->show('produto', $data);
	}

	public function dadosProduto()
	{
		$this->load->model('Produtos_Model');
		$this->load->model('Usuarios_Model');
		if(empty($_GET['codigo']) || empty($_GET['produto']))
		{
			return null;
			exit;
		}
		$produtoNome = str_replace('-', ' ', $_GET['produto']);
		$produtoId = $_GET['codigo'];
		$produto = $this->Produtos_Model->listaProdutos(null, $produtoId, $produtoNome);
		if(!empty($produto))
		{
			foreach ($produto as $prod) {
				$qtdeVendas = $this->Usuarios_Model->qtdeVendas($prod['usuarioId']);
				if (!empty($prod['produtoValorPromocao'])) {
					$promocao = '
					<span class="detail-price-old">
									R$' . $prod['produtoValor'] . '
								</span>
					<span class="detail-price">
					R$' . $prod['produtoValorPromocao'] . '
						</span>
					';
					
				} else {
					$promocao = '
					<span class="detail-price">
					R$' . $prod['produtoValor'] . '
						</span>
					';
				}
				if (!empty($prod['produtoImagem1'])) {
					$produto1 = '
					<div class="col-sm-2 col-3">
									<div class="img-small border" style="background-image: url(' . base_url() .$prod['produtoImagem1']. ')" data-src="' . base_url() .$prod['produtoImagem1']. '"></div>
							</div>
					';
				}
				else
				{
					$produto1 = "";
				}
				if (!empty($prod['produtoImagem2'])) {
					$produto2 = '
					<div class="col-sm-2 col-3">
									<div class="img-small border" style="background-image: url(' . base_url() .$prod['produtoImagem2']. ')" data-src="' . base_url() .$prod['produtoImagem2']. '"></div>
							</div>
					';
				}
				else
				{
					$produto2 = "";
				}
				$row[] = '
				<div class="col-12 bg-white py-3 my-3">
				<div class="row">
	
				
					<div class="col-lg-5 col-md-12 mb-3">
						<div class="col-12 mb-3">
							<div class="img-large border" style="background-image: url(' . base_url() .$prod['produtoImagemPrincipal']. '"></div>
						</div>
						<div class="col-12">
							<div class="row">
								<div class="col-sm-2 col-3">
									<div class="img-small border" style="background-image: url(' . base_url() .$prod['produtoImagemPrincipal']. ')" data-src="' . base_url() . $prod['produtoImagemPrincipal'].'"></div>
								</div>
								'.$produto1.'
								'.$produto2.'
								
								
							</div>
						</div>
					</div>
					
					<div class="col-lg-5 col-md-9">
						<div class="col-12 product-name large">
							' . $prod['produtoNome'] . '
							<!--<small>De <a href="#">Dell</a></small>-->
						</div>
						<div class="col-12 px-0">
							<hr>
						</div>
						<div class="col-12">
							<ul>
								<li>'.$prod['produtoDescricao'].'</li>
							</ul>
						</div>
					</div>
					<!-- Product Info -->
	
					<!-- Sidebar -->
					<div class="col-lg-2 col-md-3 text-center">
						<div class="col-12 border-left border-top sidebar h-100">
							<div class="row">
								<div class="col-12">
								'.$promocao.'
								</div>
								<div class="col-12 mt-3">
									<button class="btn btn-outline-dark" type="button"><i class="fas fa-handshake mr-2"></i>Negociar</button>
								</div>
								<div class="col-12 mt-3">
									<button class="btn btn-outline-secondary btn-sm produto_favorito"  type="button" produto='.$prod['produtoId'].' chave='.$prod['produtoHash'].'><i class="fas fa-heart mr-2"></i>Favorito</button>
								</div>
							</div>
						</div>
					</div>
					<!-- Sidebar -->
	
				</div>
			</div>
			<div class="col-12 mb-3 py-3 bg-white text-justify">
			<div class="row">
	
				<!-- Details -->
				<div class="col-md-12">
					<div class="col-12">
						<div class="row">
							<div class="col-12 text-uppercase">
								<h2><u>Informações do vendedor</u></h2>
							</div>
							<div class="col-6" id="details">
								<img  src="'.base_url() .$prod['usuarioFoto'].'" class="ff">
								<div class="mt-2">
								<p><b>Nome:</b>'.$prod['usuarioNome'].'</p>
								<p><b>Endereço:</b>'.$prod['usuarioRua'].', Nº'.$prod['usuarioNumero'].', '.$prod['usuarioBairro'].'</p>
								<p><b>Localização:</b>'.$prod['usuarioCidade'].' '.$prod['usuarioEstado'].'</p>
								<p><b>Quantidade de Vendas:</b> '.$qtdeVendas.'</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Details -->
	
			  
	
			</div>
		</div>
				';
				$similares = $this->Produtos_Model->listaProdutosSimilares($prod['subCategoriaId'],$prod['produtoId']);
				if(!empty($similares))
				{
					$row[]='
					<div class="col-12">
					<div class="row">
						<div class="col-12 py-3">
							<div class="row">
								<div class="col-12 text-center text-uppercase">
									<h2>Produtos Similares</h2>
								</div>
							</div>
							<div class="row">
					';
					foreach ($similares as $similar) {
						if (!empty($similar['produtoValorPromocao'])) {
							$promocao = '
							<span class="product-price-old">
											' . $similar['produtoValor'] . '
										</span>
									<br>
							<span class="product-price">
							R$'.$similar['produtoValorPromocao'].'
								</span>
							';
						}
						else
						{
							$promocao = '
							<span class="product-price">
							R$'.$similar['produtoValor'].'
								</span>
							';
						}
						$titulo_novo = strtolower(preg_replace(
							"/[ -]+/",
							"-",
							strtr(
								utf8_decode(trim($similar['produtoNome'])),
								utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),
								"aaaaeeiooouuncAAAAEEIOOOUUNC-"
							)
						));
						$row[] = '
						<div class="col-lg-3 col-sm-6 my-3">
						<div class="col-12 bg-white text-center h-100 product-item">
							<div class="row h-100">
								<div class="col-12 p-0 mb-3">
									<a href="'.base_url().'produto?produto='.$titulo_novo.'&codigo='.$similar['produtoId'].'">
										<img src="'.base_url().'public/img/image-4.jpg" class="img-fluid">
									</a>
								</div>
								<div class="col-12 mb-3">
									<a href="#" class="product-name">'.$similar['produtoNome'].'</a>
								</div>
								<div class="col-12 mb-3">
									'.$promocao.'
								</div>
							   
							</div>
						</div>
					</div>
						';
					}
					$row[]='
					</div>
								</div>
							</div>
						</div>
					';
				}
				
			}
			return $row;
		}
		else
		{
			return null;
		}
		
	}
}
