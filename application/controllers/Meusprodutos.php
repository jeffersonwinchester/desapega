<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Meusprodutos extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library("session");
	}

	public function index()
	{
		if ($this->session->userdata("usuario_id")) {
			$this->load->model('Funcoes_Genericas');
			$this->load->model('Favoritos_Model');
			$this->load->model('Produtos_Model');
			$data =  array(
				"scripts" => array(
					"tabela.js",
					"produtosalteracao.js"
				),
				"titulo" => "Meus Produtos",
				"menu" => $this->Funcoes_Genericas->constroiMenu(),
				"usuario" => $this->session->userdata("usuario_id"),
				"numerofavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"), 1),
				"listagemfavoritos" => $this->Favoritos_Model->retornaFavoritos($this->session->userdata("usuario_id"), null),
				"meusprodutos" => $this->Produtos_Model->listaProdutosUsuario($this->session->userdata("usuario_id"), $this->session->userdata("usuario_hash"))

			);

			$this->template->show('meusprodutos', $data);
		} else {
			$data2 =  array(

				"mensagem" => "Area Restrita, faça login para continuar!"

			);
			$this->load->view('login', $data2);
		}
	}


	public function ajax_remove_produto()
	{
		if (!$this->input->is_ajax_request()) {
			exit("Nenhum acesso de script direto permitido!");
		}
		$json = array();
		$json["status"] = 1;
		$json["erros"] = array();


		$this->load->model("Produtos_Model");
		$data = $this->input->post();
		$user = $this->session->userdata("usuario_id");
		$verificaproduto = $this->Produtos_Model->VerificaProduto($data["produto"], $data["chave"], $data["codigo"]);
		if (empty($verificaproduto)) {
			$json["status"] = 0;
			$json["erros"] = "Produto não existe!";
			echo json_encode($json);
			exit;
		} else {
			$verificaproduto = $verificaproduto[0];
		}

		if ($data["codigo"] != $user) {
			$json["status"] = 0;
			$json["erros"] = "Erro, você não tem permissão para apagar esse produto!";
			echo json_encode($json);
			exit;
		}
		$caminho = str_replace('\\', '/', getcwd());
		$pasta = $caminho . "/public/img/produtos/" . $data["produto"];
		$this->deletar($pasta);

		$this->Produtos_Model->removeProduto($data["produto"], $data["chave"]);


		echo json_encode($json);
	}
	function deletar($pasta)
	{

		$iterator     = new RecursiveDirectoryIterator($pasta, FilesystemIterator::SKIP_DOTS);
		$rec_iterator = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::CHILD_FIRST);

		foreach ($rec_iterator as $file) {
			$file->isFile() ? unlink($file->getPathname()) : rmdir($file->getPathname());
		}

		rmdir($pasta);
	}

	public function ajax_cadastra_formulario_venda()
    {
        if (!$this->input->is_ajax_request()) {
            exit("Nenhum acesso de script direto permitido!");
        }

        $json = array();
        $json["status"] = 1;
        $json["erros"] = array();
        $data = $this->input->post();
        $this->load->model("Produtos_Model");
        $this->load->library('form_validation');
        $this->form_validation->set_rules(
            'vendaSite',
            'venda',
            'required',
            array('required' => 'Local da venda é obrigatório')
        );
        
        if ($this->form_validation->run() == FALSE) {
            $erros = array('mensagens' => validation_errors());
            foreach ($erros as $erro) {
                $retirada = array("</p>", "<p>");
                $json["erros"] = str_replace($retirada, '', $erro);
			}
			$json["status"] = 0;
            echo json_encode($json);
            exit;
		}
		$data["usuarioId"] = $this->session->userdata("usuario_id");
		
		

	
		$this->Produtos_Model->cadastraVenda($data);

		echo json_encode($json);
	}

}
