$(function () {
 
 /*Funçao para remover um produto*/
 $(document).on('click', '.btn_exclui_produto', function () {
    produto = $(this).attr("produto");
    chave = $(this).attr("chave");
    codigo = $(this).attr("codigo");
    Swal.fire({
        title: "Atenção!",
        text: "Deseja excluir esse produto?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: "#d9534f",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: true,
        closeOnCancel: true,
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: BASE_URL + "meusprodutos/ajax_remove_produto",
                dataType: "json",
                data: {"produto": produto, "chave": chave,"codigo":codigo},
                success: function(response) {
                    if(response["status"])
                    {
                        Swal.fire(
                            'Sucesso!',
                            'Produto excluido com sucesso.',
                            'success'
                            )
                            $("#tabela_usuarios").load(BASE_URL + "meusprodutos #tabela_usuarios");
                            $("#modal_avaliacao").modal({backdrop: 'static', keyboard: false});
                        
                    }
                    else
                    {
                        Swal.fire({
                            icon: 'error',
                            title: 'Erro ao excluir!',
                            text: response["erros"]

                        })
                    }
                    

                }
            })
        }
    })
});


$("#formulario_questionario").submit(function() {

    $.ajax({
            type: "POST",
            url: BASE_URL + "meusprodutos/ajax_cadastra_formulario_venda",
            dataType: "json",
            data: $(this).serialize(),
            success: function(response) {
                if (response["status"]) {
                    $("#modal_avaliacao").modal('hide');
                    Swal.fire(
                        'Sucesso!',
                        'Obrigado por responder nosso questionário!',
                        'success'
                        )
                    $("#formulario_questionario")[0].reset(); 
                  
                    
                    
                    
                } else {
                    
                    Swal.fire({
                        icon: 'error',
                        title: 'Erro !',
                        text: response["erros"]
    
                    })
                  
                }
    
            }
        })
    
        return false;
    });
    



});