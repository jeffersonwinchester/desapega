$(function(){

    $('#info').css('display','none');



    //Alterando a Senha de um usuario
$("#form_esqueceu_senha").submit(function() {

    $.ajax({
            type: "POST",
            url: BASE_URL + "alterasenha/ajax_esqueceu_senha",
            dataType: "json",
            data: $(this).serialize(),
            success: function(response) {
                if (response["status"]) {
                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success'
                        },
                        buttonsStyling: false
                    })

                    swalWithBootstrapButtons.fire({
                        title: 'Senha alterada com sucesso!',
                        text: "Você será redirecionado para a página de login!",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'Confirmar',
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                        allowOutsideClick: false,
                        reverseButtons: true
                    }).then((result) => {
                        if (result.value) {
                            window.location = BASE_URL + "login"; 
                        } 
                    })
                    /*Swal.fire(
                        
                        'Sucesso!',
                        'Senha alterada com sucesso!',
                        'success'
                        )  
                        $("#form_esqueceu_senha")[0].reset(); */   
                    
                } else {
                    
                    Swal.fire({
                        icon: 'error',
                        title: 'Erro ao realizar cadastro!',
                        text: response["erros"]
    
                    })
                    
                }
    
            }
        })
    
        return false;
    });
    
});