$(function () {
    /*Funçao para adicionar um produto aos favorios*/
    $(document).on('click', '.produto_favorito', function () {
        produto = $(this).attr("produto");
        chave = $(this).attr("chave");
        $.ajax({
            type: "POST",
            url: BASE_URL + "favoritos/ajax_adiciona_favorito",
            dataType: "json",
            data: { "produto": produto, "chave": chave },
            success: function (response) {
                if (response["status"]) {
                    Swal.fire(
                        'Sucesso!',
                        'Produto adicionado aos favoritos!',
                        'success'
                    )
                    $("#favorito_atualiza").load(BASE_URL + "produto #favorito_atualiza");

                } else {

                    Swal.fire({
                        icon: 'error',
                        title: 'Erro ao adicionar aos favoritos!',
                        text: response["erros"]

                    })
                }
            }
        })
    });

     /*Funçao para remover um produto da lista de favoritos*/
     $(document).on('click', '.remover_favorito', function () {
        produto = $(this).attr("produto");
        chave = $(this).attr("chave");
        favorito = $(this).attr("favorito");
        Swal.fire({
			title: "Atenção!",
			text: "Deseja remover esse favorito?",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: "#d9534f",
			confirmButtonText: "Sim",
			cancelButtonText: "Não",
			closeOnConfirm: true,
			closeOnCancel: true,
		}).then((result) => {
			if (result.value) {
				$.ajax({
					type: "POST",
					url: BASE_URL + "favoritos/ajax_remove_favorito",
					dataType: "json",
					data: {"produto": produto, "chave": chave,"favorito":favorito},
					success: function(response) {
						if(response["status"])
						{
							Swal.fire(
								'Sucesso!',
								'Favorito excluido com sucesso.',
								'success'
                                )
                                $("#favorito_atualiza").load(BASE_URL + "favoritos #favorito_atualiza");
                                $("#tabela_favoritos").load(BASE_URL + "favoritos #tabela_favoritos");
							
						}
						else
						{
							Swal.fire({
								icon: 'error',
								title: 'Erro ao excluir!',
								text: response["erros"]

							})
						}
						

					}
				})
			}
		})
    });

    $("#produto_promocao1").change(function(){ 
        if( $(this).is(":checked") ){ 
            $("#promocao").html('<br><div class="form-row">\
            <div class="form-group col-md-4 ">\
              <label for="inputCity">Valor do Produto na promoção:</label>\
              <div class="iconInput">\
                <i class="fa fa-dollar-sign"></i>\
                <input type="text" name="produtoValorPromocao" id="produtoValorPromocao" class="form-control" placeholder="Digite o valor do produto na promoção">\
              </div>\
            </div>\
          </div>');
          $("#produtoValorPromocao").maskMoney({thousands:'.', decimal:',', symbolStay: true});
        }
    });

    $("#produto_promocao0").change(function(){ 
        if( $(this).is(":checked") ){ 
            $("#promocao").html("");
        }
    });

    $(document).on('blur', '#produtoValorPromocao', function () {
        var precoOriginal = $("#produtoValor").val();
        var promocao = $(this).val();
        if(promocao>precoOriginal)
        {
            Swal.fire({
                icon: 'error',
                title: 'Erro de valores!',
                text: 'O preço da promoção deve ser menor que o valor original!'

            })
            $(this).val("");
        }
    
    });
    
    $("#produtoValor").maskMoney({ thousands:'.', decimal:',', symbolStay: true});

    $("#produtoCategoria").change(function(){ 
       categoria = $(this).val();
       $.ajax({
        type: "POST",
        url: BASE_URL + "cadastroproduto/ajax_lista_sub",
        dataType: "json",
        data: { "categoria": categoria },
        success: function (response) {
            if (response["status"]) {
               $("#constroi").html(response["lista"]);

            } else {

                Swal.fire({
                    icon: 'error',
                    title: 'Erro ao adicionar aos favoritos!',
                    text: response["erros"]

                })
            }
        }
    })
    });


    //Fazer upload de fotos do produto
 function uploadImgProduto(input_file,img, input_path)
 {
     produto = $(this).attr("produto");
     src_before = img.attr("src");
     img_file = input_file[0].files[0];
     form_data = new FormData();

     form_data.append("image_file", img_file);
     form_data.append("produto", produto);

     $.ajax({
         url: BASE_URL + "cadastroproduto/ajax_importa_imagem",
         dataType: "json",
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         type: "POST",
         success: function(response){
             if(response["status"])
             {
                d = new Date();
                 img.attr("src",response["img_path"]+"?"+d.getTime());
                 input_path.val(response["img_path"]);
                
                 
             }
             else
             {
                 img.attr("src", src_before);
                 input_path.siblings(".help-block").html(response["error"]);
             }
         },
         error: function()
         {
             img.attr("src", src_before);
         }

     })
 }

 $("#btn_upload_imagem_produto").change(function(){
    uploadImgProduto($(this), $("#produto_imagem_path"),$("#produtoImagemPrincipal"));
 });
 $("#btn_upload_imagem_produto1").change(function(){
    uploadImgProduto($(this), $("#produto1_imagem_path"),$("#produtoImagem1"));
 });
 $("#btn_upload_imagem_produto2").change(function(){
    uploadImgProduto($(this), $("#produto2_imagem_path"),$("#produtoImagem2"));
 });

//Cadastrando um novo produto
$("#formulario_cadastro_produto").submit(function() {

    $.ajax({
            type: "POST",
            url: BASE_URL + "cadastroproduto/ajax_cadastra_produto",
            dataType: "json",
            data: $(this).serialize(),
            beforeSend: function(){
                $("#cadastrando").html("<h5 class='text-primary'>Aguarde, cadastrando </h5>\
                    <div class='spinner-border text-primary ml-2' style='width: 20px; height: 20px;' role='status'>\
                        <span class='sr-only'>Loading...</span>\
                    </div>");
                $("#btn_salvar_produto").prop('disabled',true);
            },
            success: function(response) {
                if (response["status"]) {
                    Swal.fire(
                        'Sucesso!',
                        'O produto foi cadastrado com sucesso!',
                        'success'
                        )
                    $("#formulario_cadastro_produto")[0].reset(); 
                    $("#btn_salvar_produto").prop('disabled',false);
                    $("#cadastrando").html("");
                    $("#produto_imagem_path").attr("src","");
                    $("#produto1_imagem_path").attr("src","");
                    $("#produto2_imagem_path").attr("src","");
                    
                    
                } else {
                    
                    Swal.fire({
                        icon: 'error',
                        title: 'Erro ao realizar cadastro!',
                        text: response["erros"]
    
                    })
                    $("#cadastrando").html("");
                    $("#btn_salvar_produto").prop('disabled',false);
                }
    
            }
        })
    
        return false;
    });
    

    //Editando um produto
$("#form_edita_produto").submit(function() {

    $.ajax({
            type: "POST",
            url: BASE_URL + "editar/ajax_edita_produto",
            dataType: "json",
            data: $(this).serialize(),
            beforeSend: function(){
                $("#editando").html("<h5 class='text-primary'>Aguarde, cadastrando </h5>\
                    <div class='spinner-border text-primary ml-2' style='width: 20px; height: 20px;' role='status'>\
                        <span class='sr-only'>Loading...</span>\
                    </div>");
                $("#btn_editar_produto").prop('disabled',true);
            },
            success: function(response) {
                if (response["status"]) {
                    Swal.fire(
                        'Sucesso!',
                        'Dados do produto alterados com sucesso!',
                        'success'
                        )
                    $("#btn_editar_produto").prop('disabled',false);
                    $("#editando").html("");
                    
                    
                } else {
                    
                    Swal.fire({
                        icon: 'error',
                        title: 'Erro ao atualizar dados!',
                        text: response["erros"]
    
                    })
                    $("#editando").html("");
                    $("#btn_editar_produto").prop('disabled',false);
                }
    
            }
        })
    
        return false;
    });
    
  

});