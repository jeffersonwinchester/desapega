$(function(){
 //Fazer upload de imagem do perfil
 function uploadImgPerfil(input_file,img, input_path)
 {
     src_before = img.attr("src");
     img_file = input_file[0].files[0];
     form_data = new FormData();

     form_data.append("image_file", img_file);

     $.ajax({
         url: BASE_URL + "usuario/ajax_importa_imagem",
         dataType: "json",
         cache: false,
         contentType: false,
         processData: false,
         data: form_data,
         type: "POST",
         success: function(response){
             if(response["status"])
             {
                d = new Date();
                 img.attr("src",response["img_path"]+"?"+d.getTime());
                 input_path.val(response["img_path"]);
                 
             }
             else
             {
                 img.attr("src", src_before);
                 input_path.siblings(".help-block").html(response["error"]);
             }
         },
         error: function()
         {
             img.attr("src", src_before);
         }

     })
 }

 $("#btn_upload_imagem_perfil").change(function(){
     uploadImgPerfil($(this), $("#perfil_imagem_path"),$("#usuarioFoto"));
 });


//Cadastrando um novo usuario
$("#form_cadastra_usuario").submit(function() {

$.ajax({
        type: "POST",
        url: BASE_URL + "cadastro/ajax_cadastra_usuario",
        dataType: "json",
        data: $(this).serialize(),
        beforeSend: function(){
            $("#cadastrando").html("<h5 class='text-primary'>Aguarde, cadastrando </h5>\
                <div class='spinner-border text-primary ml-2' style='width: 20px; height: 20px;' role='status'>\
                    <span class='sr-only'>Loading...</span>\
                </div>");
            $("#btn_cadastro").prop('disabled',true);
        },
        success: function(response) {
            if (response["status"]) {
                Swal.fire(
                    'Cadastro realizado com sucesso!',
                    'Um email com sua senha inicial foi enviado para o email informado!',
                    'success'
                    )
                $("#cadastrando").html("");
                $("#form_cadastra_usuario")[0].reset(); 
                $("#btn_cadastro").prop('disabled',false);
                
                
            } else {
                
                Swal.fire({
                    icon: 'error',
                    title: 'Erro ao realizar cadastro!',
                    text: response["erros"]

                })
                $("#cadastrando").html("");
                $("#btn_cadastro").prop('disabled',false);
            }

        }
    })

    return false;
});



//Verificando login
$("#form_valida_login").submit(function() {

    $.ajax({
            type: "POST",
            url: BASE_URL + "login/ajax_efetua_login",
            dataType: "json",
            data: $(this).serialize(),
            beforeSend: function(){
                $("#logando").html("<h5 class='text-primary'>Aguarde, verificando </h5>\
                    <div class='spinner-border text-primary ml-2' style='width: 20px; height: 20px;' role='status'>\
                        <span class='sr-only'>Loading...</span>\
                    </div>");
                $("#btn-login").prop('disabled',true);
            },
            success: function(response) {
                if (response["status"]) {
                    $("#logando").html("<h5 class='text-primary'>Acessando...</h5>");
                    $("#form_valida_login")[0].reset(); 
                    $("#btn-login").prop('disabled',false);
                    window.location = response['url'];
                    
                    
                } else {
                    
                    
                    $("#logando").html("<h5 class='text-danger'>"+response["erros"]+"</h5>");
                    $("#btn-login").prop('disabled',false);
                }
    
            }
        })
    
        return false;
    });


    $("#altera_senha").click(function(){
		$("#formulario_altera_senha")[0].reset();
		$("#modal_altera_senha").modal();

    });
    

    //Atualizando um usuário
$("#formulario_editar_usuario").submit(function() {

    $.ajax({
            type: "POST",
            url: BASE_URL + "usuario/ajax_atualiza_usuario",
            dataType: "json",
            data: $(this).serialize(),
            success: function(response) {
                if (response["status"]) {
                    Swal.fire(
                        'Sucesso!',
                        'Dados foram atualizados com sucesso!',
                        'success'
                        )
                    
                    
                } else {
                    
                    Swal.fire({
                        icon: 'error',
                        title: 'Erro ao atualizar dados!',
                        text: response["erros"]
    
                    })
                    
                }
    
            }
        })
    
        return false;
    });


 //Atualizando senha de um usuário
$("#formulario_altera_senha").submit(function() {

    $.ajax({
            type: "POST",
            url: BASE_URL + "usuario/ajax_atualiza_senha",
            dataType: "json",
            data: $(this).serialize(),
            success: function(response) {
                if (response["status"]) {
                    Swal.fire(
                        'Sucesso!',
                        'Senha atualizada com sucesso!',
                        'success'
                        )
                        $("#modal_altera_senha").modal('hide');
                    
                    
                } else {
                    
                    Swal.fire({
                        icon: 'error',
                        title: 'Erro ao atualizar senha!',
                        text: response["erros"]
    
                    })
                    
                }
    
            }
        })
    
        return false;
    });


    $("#btn_solicita").click(function(){
		$("#formulario_esqueceu_senha")[0].reset();
		$("#modal_esqueceu_senha").modal();

    });


     //Atualizando senha de um usuário
$("#formulario_esqueceu_senha").submit(function() {
    $.ajax({
            type: "POST",
            url: BASE_URL + "login/ajax_esqueceu_senha",
            dataType: "json",
            data: $(this).serialize(),
            beforeSend: function(){
                $("#verificar").html("<h5 class='text-primary'>Aguarde, requisitando </h5>\
                    <div class='spinner-border text-primary ml-2' style='width: 20px; height: 20px;' role='status'>\
                        <span class='sr-only'>Loading...</span>\
                    </div>");
                $("#btn_solicita_alteracao").prop('disabled',true);
            },
            success: function(response) {
                if (response["status"]) {
                    $("#verificar").html("");
                    $("#btn_solicita_alteracao").prop('disabled',false);
                    Swal.fire(
                        'Sucesso!',
                        'Um email com um link para alteraçao de senha foi enviado para o email informado!',
                        'success'
                        )
                        $("#modal_esqueceu_senha").modal('hide');
                        
                    
                    
                } else {
                    $("#verificar").html("");
                    $("#btn_solicita_alteracao").prop('disabled',false);
                    Swal.fire({
                        icon: 'error',
                        title: 'Erro ao atualizar senha!',
                        text: response["erros"]
    
                    })
                    
                }
    
            }
        })
    
        return false;
    });

   


});